<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        App\User::create(array(
            'name' => 'Demo User',
            'email' => 'demo@rocketwist.com',
            'password' => Hash::make('demopassword')
        ));
    }
}