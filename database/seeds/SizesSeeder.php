<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class SizesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sizes')->delete();

        $timestamp = new DateTime();

        $colors = array(
            array('name' => 'XS', 'created_at' => $timestamp, 'updated_at' => $timestamp),
            array('name' => 'S', 'created_at' => $timestamp, 'updated_at' => $timestamp),
            array('name' => 'M', 'created_at' => $timestamp, 'updated_at' => $timestamp),
            array('name' => 'L', 'created_at' => $timestamp, 'updated_at' => $timestamp),
            array('name' => 'XL', 'created_at' => $timestamp, 'updated_at' => $timestamp),
        );

        DB::table('sizes')->insert($colors);
    }

}
