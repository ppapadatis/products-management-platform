{{ trans('language.reset_email_link') }}: <a href="{{ url('password/reset/'.$token) }}" target="_blank">{{ url('password/reset/'.$token) }}</a>
