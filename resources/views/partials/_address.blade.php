<div class="row {{ $class or '' }}">
    <div class="col-md-12">
        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title">{{ e(trans('language.address_title')) }}</h3>
            </div>
            <!-- /.panel-heading -->
            <div class="box-body">
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group @if ($errors->has('street')) has-error @endif">
                            {!! Form::label('street', e(trans("language.address_street")), array('class' => 'control-label')) !!}
                            {!! Form::text('address[street]', Input::old('address[street]'), array('class' => 'form-control', 'placeholder' => e(trans('language.address_street')), 'required' => 'required')) !!}
                            @if ($errors->has('street')) <p class="help-block">{{ e($errors->first('street')) }}</p> @endif
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group @if ($errors->has('number')) has-error @endif">
                            {!! Form::label('number', e(trans("language.address_number")), array('class' => 'control-label')) !!}
                            {!! Form::text('address[number]', Input::old('address[number]'), array('class' => 'form-control', 'placeholder' => e(trans('language.address_number')), 'required' => 'required')) !!}
                            @if ($errors->has('number')) <p class="help-block">{{ e($errors->first('number')) }}</p> @endif
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group @if ($errors->has('postal')) has-error @endif">
                            {!! Form::label('postal', e(trans("language.address_postal")), array('class' => 'control-label')) !!}
                            {!! Form::text('address[postal]', Input::old('address[postal]'), array('class' => 'form-control', 'placeholder' => e(trans('language.address_postal')), 'required' => 'required')) !!}
                            @if ($errors->has('postal')) <p class="help-block">{{ e($errors->first('postal')) }}</p> @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group @if ($errors->has('city')) has-error @endif">
                            {!! Form::label('city', e(trans("language.address_city")), array('class' => 'control-label')) !!}
                            {!! Form::text('address[city]', Input::old('address[city]'), array('class' => 'form-control', 'placeholder' => e(trans('language.address_city')), 'required' => 'required')) !!}
                            @if ($errors->has('city')) <p class="help-block">{{ e($errors->first('city')) }}</p> @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group @if ($errors->has('region')) has-error @endif">
                            {!! Form::label('region', e(trans("language.address_region")), array('class' => 'control-label')) !!} <em>({{ e(trans('language.form_optional')) }})</em>
                            {!! Form::text('address[region]', Input::old('address[region]'), array('class' => 'form-control', 'placeholder' => e(trans('language.address_region')))) !!}
                            @if ($errors->has('region')) <p class="help-block">{{ e($errors->first('region')) }}</p> @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group @if ($errors->has('country')) has-error @endif">
                            {!! Form::label('country', e(trans("language.address_country")), array('class' => 'control-label')) !!}
                            {!! Form::text('address[country]', Input::old('address[country]'), array('class' => 'form-control', 'placeholder' => e(trans('language.address_country')), 'required' => 'required')) !!}
                            @if ($errors->has('country')) <p class="help-block">{{ e($errors->first('country')) }}</p> @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>