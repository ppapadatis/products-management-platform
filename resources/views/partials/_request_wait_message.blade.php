<div id="domMessage" style="display:none;">
    <div class="alert alert-warning">
        <h4><i class="icon fa fa-warning"></i></h4>
        {{ e(trans('language.process_loading')) }}
    </div>
</div>