@extends('app')

@section('page-title', e(trans('language.main_title') . ': ' . trans('language.500_header')))

@section('header')
    <h1>{{ e(trans('language.500_header')) }}</h1>
@endsection

@section('content')
    <div class="error-page">
        <h2 class="headline text-red">500</h2>
        <div class="error-content">
            <h3><i class="fa fa-warning text-red"></i> {{ e(trans('language.500_title')) }}</h3>
            <p>{{ e(trans('language.500_text')) }}</p>
        </div>
    </div><!-- /.error-page -->
@endsection