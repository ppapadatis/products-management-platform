@extends('app')

@section('page-title', e(trans('language.main_title') . ': ' . trans('language.404_header')))

@section('header')
    <h1>{{ e(trans('language.404_header')) }}</h1>
@endsection

@section('content')
    <div class="error-page">
        <h2 class="headline text-yellow"> 404</h2>
        <div class="error-content">
            <h3><i class="fa fa-warning text-yellow"></i> {{ e(trans('language.404_error'))}}</h3>
            <p>{{ e(trans('language.404_reason')) }}</p>
        </div><!-- /.error-content -->
    </div><!-- /.error-page -->
@endsection