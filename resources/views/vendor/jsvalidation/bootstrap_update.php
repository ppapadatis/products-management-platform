$(function () {
    $("<?php echo $validator['selector']; ?>").validate({
        highlight: function (element) { // hightlight error inputs
            $(element).closest('.form-group, .checkbox').addClass('has-error'); // set error class to the control group
        },
        unhighlight: function (element) { // revert the change done by hightlight
            $(element).closest('.form-group, .checkbox').removeClass('has-error'); // set error class to the control group
        },
        errorElement: 'p', //default input error message container
        errorClass: 'help-block', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: ":hidden:not(select)",  // validate all fields including form hidden input
        errorPlacement: function(error, element) {
            if (element.attr("type") == "radio") {
                error.insertAfter(element.parents('div').find('.radio-list'));
            }
            else if (element.attr("type") == "checkbox") {
                error.insertAfter(element.parents('label'));
            }
            else if (element.attr("type") == "select") {
                error.insertAfter(element.parents('div').find('.chosenContainer'));
            }
            else {
                if(element.parent('.input-group').length || element.parent('.chosenContainer').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        },
        success: function (label) {
            label.closest('.form-group, .checkbox').removeClass('has-error'); // set success class to the control group
        },
        rules: <?php echo json_encode($validator['rules']); ?>,
        messages: <?php echo json_encode($validator['messages']) ?>,
        invalidHandler: function() {
            if ($('.submit').length)
                $('.submit').removeAttr('disabled').html('<?php echo e(trans("language.form_submit")) ?>');   // enables button
            if ($('.submit-password').length)
                $('.submit-password').removeAttr('disabled').html('<?php echo e(trans("language.reset_button")) ?>');   // enables button
            if ($('.submit-email').length)
                $('.submit-email').removeAttr('disabled').html('<?php echo e(trans("language.reset_password_link")) ?>');   // enables button
            if ($('.submit-login').length)
                $('.submit-login').removeAttr('disabled').html('<?php echo e(trans("language.login_button")) ?>');   // enables button
            if ($('.reset').length)
                $('.reset').removeAttr('disabled');   // enables button
        }
    });

    $.validator.addMethod("positive", function(value, element) {
        return value > 0;
    }, "<?php echo e(trans("language.form_required")); ?>");

    if ($('.positive').length) {
        $('.positive').rules("add", 'positive');
    }

    if ($('.chosen').length) {
        $('.chosen').chosen().change(function (element, value) {
            if (value.selected > 0) {
                $(this).closest('.form-group, .checkbox').removeClass('has-error'); // set error class to the control group
                $(this).parent().parent().find('.help-block').text('');
            }
        });
    }
});