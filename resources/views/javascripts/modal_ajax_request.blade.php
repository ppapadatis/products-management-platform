$(function () {
    $('#modal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        window.recipient = button.data('dataid');
    })

    $('#actionConfirm').click(function() {
        var dataId = $(this).attr('data-dataId');

        $.blockUI({ message: $('#domMessage') });

        $('#modal').modal('hide');

        $.ajax({
            url: '{{ $url }}' + window.recipient,
            type: 'DELETE',
            beforeSend: function(request) {
                return request.setRequestHeader('X-CSRF-Token', $("meta[name='token']").attr('content'));
            },
            success: function(result) {
                try {
                    delete window.recipient;
                } catch (e) {
                    window.recipient = undefined;
                }
                window.location.reload(true);
            },
            error: function(result) {
                $(document).ajaxStop($.unblockUI);
            }
        });
    });
});