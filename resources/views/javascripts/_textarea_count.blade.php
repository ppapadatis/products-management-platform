$(function () {
    var text_max = 255;
    var current = text_max - $('#description').val().length;
    $('#textarea_feedback').html(current + ' {{ e(trans('language.form_character_count')) }}');

    $('#description').keyup(function() {
        var text_length = $('#description').val().length;
        var text_remaining = text_max - text_length;

        $('#textarea_feedback').html(text_remaining + ' {{ e(trans('language.form_character_count')) }}');
    });
});