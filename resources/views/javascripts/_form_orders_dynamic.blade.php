$(function () {
    $('#submitForm').on('click', '.add-btn', function(event) {
        var nlen = $('.clonedInput:last').attr('id').replace('entry', '');

        var newNum = new Number(parseInt(nlen) + 1),
            newElem = $('#entry' + parseInt(nlen)).clone().attr('id', 'entry' + newNum).fadeIn('slow');

        // Error class
        newElem.find('.has-error').removeClass('has-error');
        newElem.find('.help-block').remove();

        // Product
        newElem.find('.product-label').attr('for', 'ID' + newNum + '_product');
        newElem.find('.product-input').removeClass('chosen positive').removeAttr('id')
            .removeAttr('aria-describedby').css('display', 'block').next().remove();
        newElem.find('.product-input').attr('id', 'ID' + newNum + '_product').attr('name', 'product' + newNum)
            .addClass('chosen positive').val('');

        // Quantity
        newElem.find('.quantity-label').attr('for', 'ID' + newNum + '_quantity');
        newElem.find('.quantity-input').removeAttr('id').removeAttr('aria-describedby').attr('id', 'ID' + newNum + '_quantity')
            .attr('name', 'quantity' + newNum).val('');

        // Color
        newElem.find('.color-label').attr('for', 'ID' + newNum + '_color');
        newElem.find('.color-input').removeAttr('id').attr('id', 'ID' + newNum + '_color').attr('name', 'color' + newNum).val('');

        // Size
        newElem.find('.size-label').attr('for', 'ID' + newNum + '_size');
        newElem.find('.size-input').removeClass('chosen positive').removeAttr('id')
            .removeAttr('aria-describedby').css('display', 'block').next().remove();
        newElem.find('.size-input').attr('id', 'ID' + newNum + '_size').attr('name', 'size' + newNum)
            .addClass('chosen positive').val('');

        // Notes
        newElem.find('.notes-label').attr('for', 'ID' + newNum + '_notes');
        newElem.find('.notes-input').removeAttr('id').attr('id', 'ID' + newNum + '_notes').attr('name', 'notes' + newNum).val('');

        $('.clonedInput:last').after(newElem);

        newElem.find('select').each(function() {
            $(this).chosen({no_results_text: "{{ e(trans('language.chosen_no_results')) }}"});
            $(this).rules("add", 'positive');
        });

        $('.remove-btn').attr('disabled', false);
    });

    $('#submitForm').on('click', '.remove-btn', function(event) {
        $(this).closest('.clonedInput').slideUp('slow', function () {
            $(this).remove();
            if ($('.clonedInput').length === 1)
                $('.remove-btn').attr('disabled', true);
            $('.add-btn').attr('disabled', false).prop('value', '<i class="fa fa-plus"></i> {{ e(trans('language.form_add_more')) }}');
        });
    });

    $('.add-btn').attr('disabled', false);

    if ($('.clonedInput').length - 1 === 0 || $('.clonedInput').length === 1)
        $('.remove-btn').attr('disabled', true);
});