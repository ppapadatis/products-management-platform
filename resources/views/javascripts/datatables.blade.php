$(function () {
    $('#dataTable').dataTable({
        "oLanguage": {
        @if (App::getLocale() == 'en')
            "sUrl": "{{ asset('plugins/datatables/lang/dataTables.english.lang') }}"
        @else
            "sUrl": "{{ asset('plugins/datatables/lang/dataTables.greek.lang') }}"
        @endif
        },
        "bPaginate": true,
        "bFilter": true,
        "bSort": true,
        "bInfo": true,
        "bAutoWidth": false,
        "iDisplayLength": 50,
        "aoColumnDefs": [{
            "bSortable": false,
            "aTargets": [ "nosort" ]
        }]
    });
});