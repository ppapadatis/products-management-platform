$(function () {
    var options = {
        state: false,
        animate: true,
        onText: '{{ e(trans("language.form_check_yes")) }}',
        onColor: 'primary',
        offText: '{{ e(trans("language.form_check_no")) }}',
        offColor: 'warning'
    };

    var cbox = $('input[name="allow"]');
    cbox.bootstrapSwitch(options);

    cbox.on('init.bootstrapSwitch', function(event) {
        $('input[name="name"]').prop('disabled', true);
        $('input[name="email"]').prop('disabled', true);
        $('input[name="password"]').prop('disabled', true);
        $('input[name="password_confirmation"]').prop('disabled', true);
        $('.submit').prop('disabled', true).html('{{ e(trans("language.form_submit")) }}');
        $('.reset').prop('disabled', true);
    });

    cbox.on('switchChange.bootstrapSwitch ', function(event, state) {
        if (state) {
            $('input[name="name"]').prop('disabled', false);
            $('input[name="email"]').prop('disabled', false);
            $('input[name="password"]').prop('disabled', false);
            $('input[name="password_confirmation"]').prop('disabled', false);
            $('.submit').prop('disabled', false).html('{{ e(trans("language.form_submit")) }}');
            $('.reset').prop('disabled', false);
        } else {
            $('input[name="name"]').prop('disabled', true);
            $('input[name="email"]').prop('disabled', true);
            $('input[name="password"]').prop('disabled', true);
            $('input[name="password_confirmation"]').prop('disabled', true);
            $('.submit').prop('disabled', true).html('{{ e(trans("language.form_submit")) }}');
            $('.reset').prop('disabled', true);
        }
    });

    $('.submit').click(function() {
        $(this).prop('disabled', true).html('<i class="fa fa-spinner fa-lg fa-spin"></i>&nbsp;{{ e(trans("language.form_submit")) }}');
        $('.reset').prop('disabled', true);
    });
});