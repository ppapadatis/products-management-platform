$(function () {
    $('.submit').prop('disabled', false).html('{{ e(trans("language.form_submit")) }}');
    $('.reset').prop('disabled', false);

    $('.submit').click(function() {
        $(this).prop('disabled', true).html('<i class="fa fa-spinner fa-lg fa-spin"></i>&nbsp;{{ e(trans("language.form_submit")) }}');
        $('.reset').prop('disabled', true);
    });
});