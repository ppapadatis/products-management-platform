$(function () {
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };

    @if (Session::get('notification_create_success'))
        toastr.success("{{ Session::get('notification_create_success') }}");

    @elseif (Session::get('notification_create_fail'))
        toastr.error("{{ Session::get('notification_create_fail') }}");

    @elseif (Session::get('notification_update_success'))
        toastr.success("{{ Session::get('notification_update_success') }}");

    @elseif (Session::get('notification_update_fail'))
        toastr.error("{{ Session::get('notification_update_fail') }}");

    @elseif (Session::get('notification_delete_success'))
        toastr.success("{{ Session::get('notification_delete_success') }}");

    @elseif (Session::get('notification_delete_fail'))
        toastr.error("{{ Session::get('notification_delete_fail') }}");

    @elseif (Session::get('status'))
        toastr.success("{{ Session::get('status') }}");
    @endif
});