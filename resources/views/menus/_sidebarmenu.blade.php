<ul class="sidebar-menu">
    <li class="header">{{ e(trans('language.sidebar_menu_navigation')) }}</li>
    <li>
        <a href="{{ url('/dashboard') }}"><i class="fa fa-home"></i> <span>{{ e(trans('language.sidebar_dashboard')) }}</span></a>
    </li>
    <li>
        <a href="{{ url('/dashboard/orders') }}"><i class="fa fa-shopping-cart"></i> <span>{{ e(trans('language.sidebar_orders')) }}</span></a>
    </li>
    <li>
        <a href="{{ url('/dashboard/customers') }}"><i class="fa fa-group"></i> <span>{{ e(trans('language.sidebar_customers')) }}</span></a>
    </li>
    <li>
        <a href="{{ url('/dashboard/products') }}"><i class="fa fa-cubes"></i> <span>{{ e(trans('language.sidebar_products')) }}</span></a>
    </li>
    <li>
        <a href="{{ url('/dashboard/transports') }}"><i class="fa fa-truck"></i> <span>{{ e(trans('language.sidebar_transports')) }}</span></a>
    </li>
    <li class="treeview">
        <a href="#"><i class="fa fa-gears"></i> <span>{{ e(trans('language.sidebar_administrator')) }}</span> <i class="fa fa-angle-left pull-right"></i></a>
        <ul class="treeview-menu">
            <li>
                <a href="{{ url('/dashboard/profile') }}"><i class="fa fa-user"></i> <span>{{ e(trans('language.sidebar_profile')) }}</span></a>
            </li>
            <li>
                <a href="#"><i class="fa fa-files-o fa-fw"></i> <span>{{ e(trans('language.sidebar_logs')) }}</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{ url('/dashboard/logs/access') }}"><i class="fa fa-sign-in"></i> <span>{{ e(trans('language.sidebar_access')) }}</span></a>
                    </li>
                    <li>
                        <a href="{{ url('/dashboard/logs/errors') }}"><i class="fa fa-warning"></i> <span>{{ e(trans('language.sidebar_error')) }}</span></a>
                    </li>
                </ul>
            </li>
        </ul>
    </li>
</ul>