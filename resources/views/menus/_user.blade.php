<!-- Sidebar user panel -->
<div class="user-panel">
    <div class="pull-left image">
        <img src="{{ Auth::getUser()->getGravatar() }}" class="img-circle" alt="User Image" />
    </div>
    <div class="pull-left info">
        <p>{{ Auth::getUser()->getAttribute('name') }}</p>
        <a href="#"><i class="fa fa-circle text-success"></i> {{ e(trans('language.main_online')) }}</a>
    </div>
</div>