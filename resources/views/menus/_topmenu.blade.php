<header class="main-header">
    <a class="logo" href="{{ url('/dashboard') }}"><b>{{ e(trans('language.main_title')) }}</b></a>
    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button -->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">{{ e(trans('language.toggle_navigation')) }}</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li>
                    <a href="{{ url('/auth/logout') }}"><i class="fa fa-sign-out"></i>&nbsp;{{ e(trans('language.user_logout')) }}</a>
                </li>
            </ul>
            <!-- /.nav -->
        </div>
    </nav>
</header>