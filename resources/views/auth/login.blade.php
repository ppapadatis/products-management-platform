@extends('app')

@section('page-title', e(trans('language.main_title')))

@section('top_css_files')
<link href="{{ asset('plugins/iCheck/square/blue.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <div class="login-box">
        <div class="login-logo">
            <a href="javascript:void(0)" style="cursor: default"><b>{{ e(trans('language.welcome')) }}</b></a>
        </div><!-- /.login-logo -->
        <div class="login-box-body">
            <p class="login-box-msg">{{ e(trans('language.session')) }}</p>
            {!! Form::open(array('role' => 'form', 'id' => 'submitForm', 'autocomplete' => 'off', 'method' => 'POST', 'url' => '/auth/login')) !!}
            <div class="form-group has-feedback @if ($errors->has('email')) has-error @endif">
                {!! Form::input('email', 'email', Input::old('email'), array(
                    'type' => 'email',
                    'class' => 'form-control',
                    'placeholder' => e(trans('language.email_login')),
                    'autofocus' => 'autofocus',
                    'required' => 'required'))
                !!}
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                @if ($errors->has('email')) <p class="help-block">{{ e($errors->first('email')) }}</p> @endif
            </div>
            <div class="form-group has-feedback @if ($errors->has('password')) has-error @endif">
                {!! Form::password('password', array(
                    'class' => 'form-control',
                    'placeholder' => e(trans('language.password_login')),
                    'required' => 'required'))
                !!}
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                @if ($errors->has('password')) <p class="help-block">{{ e($errors->first('password')) }}</p> @endif
            </div>
            <div class="form-group" align="center">
                {!! app('captcha')->display(); !!}
            </div>
            <div class="row">
                <div class="col-xs-8">
                    <div class="checkbox icheck">
                        <label>
                            {!! Form::checkbox('remember', Input::old('remember'), array('value' => 'Remember Me')) !!} {{ e(trans('language.remember_me')) }}
                        </label>
                    </div>
                </div><!-- /.col -->
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat submit-login">{{ e(trans('language.login_button')) }}</button>
                </div><!-- /.col -->
            </div>
            {!! Form::close() !!}
            <a href="{{ url('/password/email') }}">{{ e(trans('language.forgot')) }}</a><br>
        </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->
@endsection

@section('body_js_libraries')
    <script src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
@endsection

@section('body_bottom_js')
    $('[name="remember"]').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });

    $('.submit-login').click(function() {
        $(this).prop('disabled', true).html('<i class="fa fa-spinner fa-lg fa-spin"></i>&nbsp;{{ e(trans("language.login_button")) }}');
    });

    {!! $validator !!}
@endsection
