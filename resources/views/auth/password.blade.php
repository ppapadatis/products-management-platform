@extends('app')

@section('page-title', e(trans('language.main_title')))

@section('content')
    <div class="login-box">
        <div class="login-box-body">
            <p class="login-box-msg">{{ e(trans('language.reset_password')) }}</p>
            {!! Form::open(array('role' => 'form', 'id' => 'submitForm', 'autocomplete' => 'off', 'method' => 'POST', 'url' => '/password/email')) !!}
            <div class="form-group has-feedback @if ($errors->has('email')) has-error @endif">
                {!! Form::input('email', 'email', Input::old('email'), array(
                    'type' => 'email',
                    'class' => 'form-control',
                    'placeholder' => e(trans('language.email_login')),
                    'autofocus' => 'autofocus',
                    'required' => 'required'))
                !!}
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                @if ($errors->has('email')) <p class="help-block">{{ e($errors->first('email')) }}</p> @endif
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-primary btn-block btn-flat submit-email">{{ e(trans('language.reset_password_link')) }}</button>
                </div><!-- /.col -->
            </div>
            {!! Form::close() !!}
        </div><!-- /.login-box-body -->

    </div><!-- /.login-box -->
@endsection

@section('body_bottom_js')
    $('.btn-flat').removeAttr('disabled').html('{{ e(trans('language.reset_password_link')) }}');

    $('.btn-flat').click(function() {
        $(this).attr('disabled', true).html('<i class="fa fa-spinner fa-lg fa-spin"></i>&nbsp;{{ e(trans("language.reset_password_link")) }}');
    });

    {!! $validator !!}
@endsection
