@extends('app')

@section('page-title', e(trans('language.main_title')))

@section('content')
    <div class="login-box">
        <div class="login-box-body">
            <p class="login-box-msg">{{ e(trans('language.reset_password')) }}</p>
            {!! Form::open(array('role' => 'form', 'method' => 'POST', 'url' => '/password/reset')) !!}
            {!! Form::hidden('token', $token) !!}
            <div class="form-group has-feedback @if ($errors->has('email')) has-error @endif">
                {!! Form::text('email', $email, array(
                    'type' => 'email',
                    'class' => 'form-control',
                    'placeholder' => e(trans('language.email_login')),
                    'required' => 'required'))
                !!}
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                @if ($errors->has('email')) <p class="help-block">{{ e($errors->first('email')) }}</p> @endif
            </div>
            <div class="form-group has-feedback @if ($errors->has('password')) has-error @endif">
                {!! Form::password('password', array(
                    'class' => 'form-control',
                    'placeholder' => e(trans('language.password_login')),
                    'autofocus' => 'autofocus',
                    'required' => 'required'))
                !!}
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                @if ($errors->has('password')) <p class="help-block">{{ e($errors->first('password')) }}</p> @endif
            </div>
            <div class="form-group has-feedback @if ($errors->has('password_confirmation')) has-error @endif">
                {!! Form::password('password_confirmation', array(
                    'class' => 'form-control',
                    'placeholder' => e(trans('language.password_confirm_login')),
                    'required' => 'required'))
                !!}
                @if ($errors->has('password_confirmation')) <p class="help-block">{{ e($errors->first('password_confirmation')) }}</p> @endif
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-primary btn-block btn-flat submit-password">{{ e(trans('language.reset_button')) }}</button>
                </div><!-- /.col -->
            </div>
            {!! Form::close() !!}
        </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->
@endsection

@section('body_bottom_js')
    $('.btn-flat').removeAttr('disabled').html('{{ e(trans('language.reset_button')) }}');

    $('.btn-flat').click(function() {
        $(this).attr('disabled', true).html('<i class="fa fa-spinner fa-lg fa-spin"></i>&nbsp;{{ e(trans("language.reset_button")) }}');
    });

    {!! $validator !!}
@endsection