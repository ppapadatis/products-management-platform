<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="{{ asset('img/favicon.ico') }}" rel="shortcut icon">
        @if (!Auth::guest())
        <meta name="token" content="{{ csrf_token() }}">
        @endif
        <title>@yield('page-title')</title>
        @section('page-title', e(trans('language.main_title')))
        <!-- Pace -->
        <script data-pace-options='{ "ajax" : true, "document" : true, "eventLag" : true }' src="{{ asset('plugins/pace/pace.min.js') }}"></script>
        <link href="{{ asset('plugins/pace/pace.css') }}" rel="stylesheet">
        <!-- Toastr Notifications -->
        <link href="{{ asset('plugins/toastr/toastr.css') }}" rel="stylesheet">
        <!-- Bootstrap 3.3.2 -->
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- FontAwesome 4.3.0 -->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons 2.0.0 -->
        <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        @yield('top_css_files')
        <!-- Theme style -->
        <link href="{{ asset('css/AdminLTE.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link href="{{ asset('css/skins/_all-skins.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- custom css -->
            <link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css" />
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.3/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        {!! Analytics::render() !!}
    </head>
        @if (!Auth::guest())
        <body class="skin-black">
            <div class="wrapper">
            @include('menus.navigation')
                <div class="content-wrapper">
                    <section class="content-header">
                        @yield('header')
                    </section>
                    <section class="content">
                        @yield('content')
                    </section>
                </div>
                <!-- /.content-wrapper -->
                <footer class="main-footer">
                    <strong>Copyright &copy; 2015-{{ date('Y', time()) }} <a href="https://www.linkedin.com/in/papadatispanagiotis" target="_blank">Panagiotis Papadatis</a>.</strong> All rights reserved.
                </footer>
            </div>
            <!-- /.wrapper -->
        @else
        <body class="login-page">
        @yield('content')
        @endif
        <!-- jQuery 2.1.3 -->
        <script src="{{ asset('plugins/jQuery/jQuery-2.1.3.min.js') }}"></script>
        <!-- Bootstrap 3.3.2 JS -->
        <script src="{{ asset('js/bootstrap.min.js') }}" type="text/javascript"></script>
        <!-- Validate -->
        <script src="{{ asset('vendor/jsvalidation/js/jsvalidation.min.js') }}" type="text/javascript"></script>
        @if (App::getLocale() == 'el')
        <script src="{{ asset('vendor/jsvalidation/js/messages_el.min.js') }}" type="text/javascript"></script>
        @endif
        <!-- Slimscroll -->
        <script src="{{ asset('plugins/slimScroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
        <!-- FastClick -->
        <script src="{{ asset('plugins/fastclick/fastclick.js') }}"></script>
        <!-- Toastr Notifications -->
        <script src="{{ asset('plugins/toastr/toastr.js') }}"></script>
        <!-- Block UI -->
        <script src="{{ asset('plugins/blockUI/jquery.blockUI.js') }}"></script>
        @yield('body_js_libraries')
        <!-- AdminLTE App -->
        <script src="{{ asset('js/app.min.js') }}" type="text/javascript"></script>
        <!-- Notifications -->
        <script>
            $(document).ready(function() {
                @include('javascripts.notifications')
                $('.sidebar-menu a[href="' + this.location + '"]').parent().addClass('active');
                @yield('body_bottom_js')
            });
        </script>
        </body>
</html>
