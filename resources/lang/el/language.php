<?php

return [

	/**
	 * MAIN TITLE
	 */
	'main_title' => 'Διαχείριση',
	'main_online' => 'Συνδεδεμένος',

    /**
     * JAVASCRIPT
     */
    'chosen_no_results' => 'Κανένα αποτέλεσμα',

	/**
	 * LOGIN SCREEN
	 */
    'welcome' => 'Καλώς ήλθατε',
    'session' => 'Συνδεθείτε για να ξεκινήσετε τη συνεδρία σας',
	'sign_in' => 'Συνδεθείτε',
	'remember_me' => 'Απομνημόνευση',
	'email_login' => 'E-mail',
	'password_login' => 'Συνθηματικό',
	'password_confirm_login' => 'Επαλήθευση συνθηματικού',
	'login_button' => 'Σύνδεση',
    'forgot' => 'Ξέχασα το συνθηματικό μου',
    'reset_password' => 'Επαναφορά συνθηματικού',
    'reset_button' => 'Επαναφορά',
    'reset_password_link' => 'Αποστολή συνδέσμου επαναφοράς',
    'reset_password_link_send' => 'Σύνδεσμος επαναφοράς συνθηματικού σας',
    'reset_email_link' => 'Πατήστε εδώ για να επαναφέρεται το συνθηματικό σας',

    /**
     * 404
     */
    '404_header' => '404 σελίδα σφάλματος',
    '404_error' => 'Ουπς! Η σελίδα δε βρέθηκε.',
    '404_reason' => 'Λυπούμαστε, αλλά προέκυψε ένα σφάλμα. Η σελίδα που ζητήθηκε δε βρέθηκε!',

    /**
     * 500
     */
    '500_header' => '500 σελίδα σφάλματος',
    '500_title' => 'Ουπς! Κάτι πήγε στραβά.',
    '500_text' => 'Προσπαθήστε να περιηγηθείτε κάπου αλλού. Εάν το σφάλμα παραμείνει, επικοινωνήστε άμεσα με τον διαχειριστή.',

	/**
	 * TOP NAVIGATION MENU
	 */
	'toggle_navigation' => 'Μπάρα πλοήγησης',
	'user_profile' => 'Προφίλ',
	'user_settings' => 'Ρυθμίσεις',
	'user_logout' => 'Αποσύνδεση',

	/**
	 * SEARCH BAR
	 */
	'search_bar' => 'Αναζήτηση...',

    /**
     * WIDGETS
     */
    'widget_orders' => 'Παραγγελίες',
    'widget_products' => 'Προϊόντα',
    'widget_customers' => 'Πελάτες',
    'widget_transports' => 'Μεταφορικές',
    'widget_latest_orders' => 'Πρόσφατες παραγγελίες',
    'widget_place_order' => 'Προσθήκη νέας παραγγελίας',
    'widget_view_orders' => 'Προβολή παραγγελιών',
    'widget_latest_products' => 'Πρόσφατα προϊόντα',
    'widget_view_products' => 'Προβολή προϊόντων',

	/**
	 * SIDEBAR MENU
	 */
	'sidebar_dashboard' => 'Αρχική',
	'sidebar_customers' => 'Πελατολόγιο',
	'sidebar_products' => 'Προϊόντα',
	'sidebar_orders' => 'Παραγγελίες',
	'sidebar_transports' => 'Μεταφορικές',
	'sidebar_other' => 'Διάφορα',
	'sidebar_colors' => 'Χρώματα',
	'sidebar_sizes' => 'Μεγέθη',
	'sidebar_administrator' => 'Διαχειριστής',
    'sidebar_settings' => 'Ρυθμίσεις',
    'sidebar_profile' => 'Προφίλ',
    'sidebar_logs' => 'Καταγραφές',
    'sidebar_access' => 'Προσβάσεις',
    'sidebar_error' => 'Σφάλματα',
    'sidebar_menu_navigation' => 'ΠΕΡΙΗΓΗΣΗ',

    /**
     * FORM BUTTONS
     */
    'form_submit' => 'Υποβολή',
    'form_reset' => 'Επαναφορά',
    'form_cancel' => 'Ακύρωση',
    'form_optional' => 'Προαιρετικό',
    'form_edit' => 'Επεξεργασία',
    'form_delete' => 'Διαγραφή',
    'form_return' => 'Επιστροφή',
    'form_character_count' => 'χαρακτήρες υπολείπονται',
    'form_check_yes' => 'ΝΑΙ',
    'form_check_no' => 'ΟΧΙ',
    'form_allow_edit' => 'Η τροποποίηση των στοιχείων είναι απενεργοποιημένη. Ενεργοποίηση;',
    'form_add_more' => 'Προσθήκη προϊόντος',
    'form_remove_more' => 'Αφαίρεση προϊόντος',
    'form_not_empty' => 'Παρακαλώ εισάγετε μια τιμή',
    'form_required' => 'Αυτό το πεδίο είναι υποχρεωτικό.',
    'form_view' => 'Προβολή',

    /**
	 * MODAL
	 */
	'modal_ok' => 'ΟΚ',
	'modal_delete' => 'Διαγραφή',
	'modal_cancel' => 'Ακύρωση',
	'modal_delete_title' => 'Επιβεβαίωση διαγραφής',
	'modal_delete_body_customer' => 'Είστε σίγουρος πως θέλετε να διαγράψετε το συγκεκριμένο πελάτη;',
	'modal_delete_body_product' => 'Είστε σίγουρος πως θέλετε να διαγράψετε το συγκεκριμένο προϊόν;',
	'modal_delete_body_size' => 'Είστε σίγουρος πως θέλετε να διαγράψετε το συγκεκριμένο μέγεθος;',
	'modal_delete_body_color' => 'Είστε σίγουρος πως θέλετε να διαγράψετε το συγκεκριμένο χρώμα;',
	'modal_delete_body_transport' => 'Είστε σίγουρος πως θέλετε να διαγράψετε τη συγκεκριμένη μεταφορική;',
	'modal_delete_body_order' => 'Είστε σίγουρος πως θέλετε να διαγράψετε τη συγκεκριμένη παραγγελία;',
	'modal_more_info' => 'Περισσότερα',

    /**
     * WAIT REQUEST
     */
    'process_loading' => 'Το αίτημά σας είναι υπό επεξεργασία. Παρακαλώ περιμένετε...',

	/**
	 * NOTIFICATIONS
	 */
	'notification_create_success' => 'Η εγγραφή δημιουργήθηκε επιτυχώς!',
	'notification_create_fail' => 'Η εγγραφή δεν αποθηκεύθηκε.',
	'notification_update_success' => 'Η εγγραφή ενημερώθηκε επιτυχώς!',
	'notification_update_fail' => 'Η εγγραφή δεν ενημερώθηκε.',
	'notification_delete_success' => 'Η εγγραφή διαγράφηκε επιτυχώς!',
	'notification_delete_fail' => 'Η εγγραφή δεν διαγράφηκε.',

	/**
	 * CUSTOMERS PAGE
	 */
	'customers_table' => 'Εγγραφές',
	'customers_name' => 'Ονοματεπώνυμο',
	'customers_fname' => 'Όνομα',
	'customers_lname' => 'Επώνυμο',
	'customers_distTitle' => 'Διακριτικός τίτλος',
	'customers_afm' => 'Α.Φ.Μ.',
	'customers_doy' => 'Δ.Ο.Υ.',
	'customers_phone' => 'Τηλέφωνο #1',
	'customers_phone_view' => 'Τηλέφωνο',
	'customers_phone_sec' => 'Τηλέφωνο #2',
	'customers_email' => 'Email',
	'customers_address' => 'Διεύθυνση',
	'customers_transport' => 'Μεταφορική εταιρεία',
	'customers_empty_warning' => 'Δεν υπάρχουν διαθέσιμες εγγραφές',
	'customers_add_new' => 'Προσθήκη νέου πελάτη',
	'customers_edit' => 'Επεξεργασία πελάτη',
	'customers_form_new' => 'Προσθήκη πελάτη',
    'customers_not_found_warning' => 'Ο πελάτης που ζητήθηκε δεν υπάρχει στο σύστημα.',

    /**
     * PRODUCTS PAGE
     */
    'products_table' => 'Εγγραφές',
    'products_sku' => 'Κωδικός',
    'products_description' => 'Περιγραφή',
    'products_fabric' => 'Υλικό',
    'products_wholesale_price' => 'Τιμή χονδρικής',
    'products_empty_warning' => 'Δεν υπάρχουν διαθέσιμες εγγραφές',
    'products_add_new' => 'Προσθήκη νέου προϊόντος',
    'products_form_new' => 'Προσθήκη προϊόντος',
    'products_edit' => 'Επεξεργασία προϊόντος',
    'products_not_found_warning' => 'Το προϊόν που ζητήθηκε δεν υπάρχει στο σύστημα.',

    /**
     * TRANSPORTS PAGE
     */
    'transports_table' => 'Εγγραφές',
    'transports_empty_warning' => 'Δεν υπάρχουν διαθέσιμες εγγραφές',
    'transports_add_new' => 'Προσθήκη νέας μεταφορικής',
    'transports_form_new' => 'Προσθήκη μεταφορικής',
    'transports_edit' => 'Επεξεργασία μεταφορικής',
    'transports_not_found_warning' => 'Η μεταφορική που ζητήθηκε δεν υπάρχει στο σύστημα.',
    'transports_name' => 'Επωνυμία',
    'transports_phone' => 'Τηλέφωνο',
    'transports_city' => 'Έδρα',

    /**
     * ACCESS LOGS
     */
    'accesslogs_title' => 'Καταγραφές προσβάσεων',
    'accesslogs_empty_warning' => 'Δεν υπάρχουν διαθέσιμες εγγραφές',
    'accesslogs_remote_ip' => 'Διεύθυνση IP',
    'accesslogs_user' => 'Χρήστης',
    'accesslogs_date' => 'Ημερομηνία',
    'accesslogs_status' => 'Κατάσταση',
    'accesslogs_status_ok' => 'ΕΠΙΤΥΧΙΑ',
    'accesslogs_status_not_ok' => 'ΑΠΟΤΥΧΙΑ',

    /**
     * ERROR LOGS
     */
    'errorlogs_title' => 'Καταγραφές σφαλμάτων',
    'errorlogs_error' => 'Σφάλμα',
    'errorlogs_severity' => 'Δριμύτητα',
    'errorlogs_id' => 'Αναγνωριστικό',
    'errorlogs_details' => 'Λεπτομέρειες σφάλματος',
    'errorlogs_user_agent' => 'Πράκτορας',
    'errorlogs_remote_ip' => 'Διεύθυνση',
    'errorlogs_user' => 'Χρήστης',
    'errorlogs_stacktrace' => 'Ίχνος',
    'errorlogs_read_more' => 'Περισσότερα',
    'errorlogs_empty_warning' => 'Δεν υπάρχουν διαθέσιμες εγγραφές',

    /**
     * ADDRESS
     */
    'address_title' => 'Διεύθυνση',
    'address_street' => 'Οδός',
    'address_number' => 'Αριθμός',
    'address_postal' => 'Τ.Κ.',
    'address_city' => 'Πόλη',
    'address_region' => 'Γεωγραφικό δια/σμα',
    'address_country' => 'Χώρα',

    /**
     * PROFILE
     */
    'profile_name' => 'Ονοματεπώνυμο',
    'profile_email' => 'Διεύθυνση e-mail',
    'profile_password' => 'Συνθηματικό',
    'profile_password_repeat' => 'Επανάληψη συνθηματικού',
    'profile_manage' => 'Τροποποίηση χρήστη',

    /**
     * ORDERS
     */
    'orders_empty_warning' => 'Δεν υπάρχουν διαθέσιμες εγγραφές',
    'orders_order_id' => 'ΑΑ παραγγελίας',
    'orders_order' => 'Παραγγελία',
    'orders_customer' => 'Ονοματεπώνυμο πελάτη',
    'orders_discount' => 'Έκπτωση',
    'orders_vat' => 'Φ.Π.Α.',
    'orders_transport' => 'Μεταφορική εταιρεία',
    'orders_product' => 'Προϊόν',
    'orders_quantity' => 'Ποσότητα',
    'orders_color' => 'Χρώμα',
    'orders_size' => 'Μέγεθος',
    'orders_notes' => 'Σημειώσεις',
    'orders_date' => 'Ημερομηνία',
    'orders_table' => 'Εγγραφές',
    'orders_add_new' => 'Προσθήκη νέας παραγγελίας',
    'orders_form_new' => 'Προσθήκη παραγγελίας',
    'orders_not_found_warning' => 'Η παραγγελία που ζητήθηκε δεν υπάρχει στο σύστημα.',
    'orders_edit' => 'Επεξεργασία παραγγελίας',

];
