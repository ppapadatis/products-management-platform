<?php

return [

	/**
	 * MAIN TITLE
	 */
	'main_title' => 'Management',
    'main_online' => 'Online',

    /**
     * JAVASCRIPT
     */
    'chosen_no_results' => 'No results match',

	/**
	 * LOGIN SCREEN
	 */
    'welcome' => 'Welcome',
    'session' => 'Sign in to start your session',
	'sign_in' => 'Sign In',
	'remember_me' => 'Remember me',
	'email_login' => 'E-mail',
	'password_login' => 'Password',
    'password_confirm_login' => 'Password confirmation',
	'login_button' => 'Login',
    'forgot' => 'I forgot my password',
    'reset_password' => 'Reset password',
    'reset_button' => 'Reset',
    'reset_password_link' => 'Send password reset link',
    'reset_password_link_send' => 'Your password reset link',
    'reset_email_link' => 'Click here to reset your password',

    /**
     * 404
     */
    '404_header' => '404 error page',
    '404_error' => 'Oops! Page not found.',
    '404_reason' => 'Sorry, an error has occured. Requested page not found!',

    /**
     * 500
     */
    '500_header' => '500 error page',
    '500_title' => 'Oops! Something went wrong.',
    '500_text' => 'Try to navigate somewhere else. If the error persists, contact an administrator.',

	/**
	 * TOP NAVIGATION MENU
	 */
	'toggle_navigation' => 'Toggle navigation',
	'user_profile' => 'User profile',
	'user_settings' => 'Settings',
	'user_logout' => 'Logout',

	/**
	 * SEARCH BAR
	 */
	'search_bar' => 'Search...',

    /**
     * WIDGETS
     */
    'widget_orders' => 'Orders',
    'widget_products' => 'Products',
    'widget_customers' => 'Customers',
    'widget_transports' => 'Transport companies',
    'widget_latest_orders' => 'Latest orders',
    'widget_place_order' => 'Place new order',
    'widget_view_orders' => 'View all orders',
    'widget_latest_products' => 'Recently added products',
    'widget_view_products' => 'View all products',

	/**
	 * SIDEBAR MENU
	 */
	'sidebar_dashboard' => 'Dashboard',
	'sidebar_customers' => 'Customers',
    'sidebar_products' => 'Products',
    'sidebar_orders' => 'Order',
    'sidebar_transports' => 'Transports',
    'sidebar_other' => 'Other',
    'sidebar_colors' => 'Colors',
    'sidebar_sizes' => 'Sizes',
    'sidebar_administrator' => 'Administrator',
    'sidebar_settings' => 'Settings',
    'sidebar_profile' => 'Profile',
    'sidebar_logs' => 'Logs',
    'sidebar_access' => 'Access',
    'sidebar_error' => 'Errors',
    'sidebar_menu_navigation' => 'MAIN NAVIGATION',

    /**
     * FORM BUTTONS
     */
    'form_submit' => 'Submit',
    'form_reset' => 'Reset',
    'form_cancel' => 'Cancel',
    'form_optional' => 'Optional',
    'form_edit' => 'Edit',
    'form_delete' => 'Delete',
    'form_return' => 'Return',
    'form_character_count' => 'characters remaining',
    'form_check_yes' => 'ON',
    'form_check_no' => 'OFF',
    'form_allow_edit' => 'Modification of information is disabled. Enable?',
    'form_add_more' => 'Add a product',
    'form_remove_more' => 'Remove a product',
    'form_not_empty' => 'Please enter a value',
    'form_required' => 'This field is required.',
    'form_view' => 'View',

    /**
	 * MODAL
	 */
	'modal_ok' => 'OK',
    'modal_delete' => 'Delete',
	'modal_cancel' => 'Cancel',
	'modal_delete_title' => 'Delete confirmation',
	'modal_delete_body_customer' => 'Are you sure you want to delete the selected customer?',
    'modal_delete_body_product' => 'Are you sure you want to delete the selected product?',
    'modal_delete_body_size' => 'Are you sure you want to delete the selected size?',
    'modal_delete_body_color' => 'Are you sure you want to delete the selected color?',
    'modal_delete_body_transport' => 'Are you sure you want to delete the selected transport company?',
    'modal_delete_body_order' => 'Are you sure you want to delete the selected order?',
    'modal_more_info' => 'More info',

    /**
     * WAIT REQUEST
     */
    'process_loading' => 'We are processing your request.  Please be patient...',

	/**
	 * NOTIFICATIONS
	 */
	'notification_create_success' => 'Record has been created successfully!',
	'notification_create_fail' => 'Record could not be saved.',
	'notification_update_success' => 'Record has been updated successfully!',
	'notification_update_fail' => 'Record could not be updated.',
    'notification_delete_success' => 'Record has been deleted successfully!',
    'notification_delete_fail' => 'Record could not be deleted.',

	/**
	 * CUSTOMERS PAGE
	 */
	'customers_table' => 'Records',
	'customers_name' => 'Name',
    'customers_fname' => 'First name',
    'customers_lname' => 'Last name',
	'customers_distTitle' => 'Distinctive title',
	'customers_afm' => 'TRN',
	'customers_doy' => 'Tax authority',
	'customers_phone' => 'Phone #1',
	'customers_phone_view' => 'Phone',
	'customers_phone_sec' => 'Phone #2',
	'customers_email' => 'Email',
	'customers_transport' => 'Transport company',
	'customers_address' => 'Address',
	'customers_empty_warning' => 'No records available',
	'customers_add_new' => 'Add new customer',
	'customers_edit' => 'Edit customer',
	'customers_form_new' => 'Customer creation',
	'customers_not_found_warning' => 'The customer you are requesting for does not exist.',

    /**
     * PRODUCTS PAGE
     */
    'products_table' => 'Records',
    'products_sku' => 'SKU',
    'products_description' => 'Description',
    'products_fabric' => 'Fabric',
    'products_wholesale_price' => 'Wholesale price',
    'products_empty_warning' => 'No records available',
    'products_add_new' => 'Add new product',
    'products_form_new' => 'Product creation',
    'products_edit' => 'Edit product',
    'products_not_found_warning' => 'The product you are requesting for does not exist.',

    /**
     * TRANSPORTS PAGE
     */
    'transports_table' => 'Records',
    'transports_empty_warning' => 'No records available',
    'transports_add_new' => 'Add new transport company',
    'transports_form_new' => 'Transport company creation',
    'transports_edit' => 'Edit transport company',
    'transports_not_found_warning' => 'The transport company you are requesting for does not exist.',
    'transports_name' => 'Business name',
    'transports_phone' => 'Phone',
    'transports_city' => 'City',

    /**
     * ACCESS LOGS
     */
    'accesslogs_title' => 'Access logs',
    'accesslogs_empty_warning' => 'No records available',
    'accesslogs_remote_ip' => 'Remote IP',
    'accesslogs_user' => 'User',
    'accesslogs_date' => 'Date',
    'accesslogs_status' => 'Status',
    'accesslogs_status_ok' => 'SUCCESS',
    'accesslogs_status_not_ok' => 'FAILURE',

    /**
     * ERROR LOGS
     */
    'errorlogs_title' => 'Error logs',
    'errorlogs_error' => 'Error',
    'errorlogs_severity' => 'Severity',
    'errorlogs_id' => 'ID',
    'errorlogs_details' => 'Error details',
    'errorlogs_user_agent' => 'User Agent',
    'errorlogs_remote_ip' => 'Remote IP',
    'errorlogs_user' => 'User',
    'errorlogs_stacktrace' => 'Stacktrace',
    'errorlogs_read_more' => 'Read more',
    'errorlogs_empty_warning' => 'No records available',

    /**
     * ADDRESS
     */
    'address_title' => 'Address',
    'address_street' => 'Street',
    'address_number' => 'Number',
    'address_postal' => 'Postal code',
    'address_city' => 'City',
    'address_region' => 'Region',
    'address_country' => 'Country',

    /**
     * PROFILE
     */
    'profile_name' => 'Full name',
    'profile_email' => 'E-mail',
    'profile_password' => 'Password',
    'profile_password_repeat' => 'Repeat password',
    'profile_manage' => 'Edit user information',

    /**
     * ORDERS
     */
    'orders_empty_warning' => 'No records available',
    'orders_order_id' => 'Order ID',
    'orders_order' => 'Order',
    'orders_customer' => ' Customer',
    'orders_discount' => 'Discount',
    'orders_vat' => 'V.A.T.',
    'orders_date' => 'Date',
    'orders_transport' => 'Transport company',
    'orders_product' => 'Product',
    'orders_quantity' => 'Quantity',
    'orders_color' => 'Color',
    'orders_size' => 'Size',
    'orders_notes' => 'Notes',
    'orders_table' => 'Records',
    'orders_add_new' => 'Place new order',
    'orders_form_new' => 'Order creation',
    'orders_not_found_warning' => 'The order you are requesting for does not exist.',
    'orders_edit' => 'Edit order',

];
