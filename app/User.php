<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'email', 'password'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

    public function errorLogs()
    {
        return $this->hasMany('\App\Modules\Core\Models\ErrorLog', 'user_id', 'id');
    }

    public function accessLogs()
    {
        return $this->hasMany('\App\Modules\Core\Models\AccessLog', 'user_id', 'id');
    }

    public function getGravatar()
    {
        $hash = md5(strtolower(trim($this->attributes['email'])));
        return 'http://www.gravatar.com/avatar/' . $hash;
    }

    public static function getInfoByResetToken($token)
    {
        return \DB::table('users')
            ->join('password_resets', 'users.email', '=', 'password_resets.email')
            ->select('users.email')
            ->where('password_resets.token', '=', $token)
            ->first();
    }

}
