<?php

// Home
Breadcrumbs::register('home', function($breadcrumbs)
{
    $breadcrumbs->push(e(trans('language.sidebar_dashboard')), URL::to('dashboard'));
});

// Home > Orders
Breadcrumbs::register('orders', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push(e(trans('language.sidebar_orders')), URL::to('dashboard/orders'));
});

// Home > Orders > Create
Breadcrumbs::register('orders_create', function($breadcrumbs)
{
    $breadcrumbs->parent('orders');
    $breadcrumbs->push(e(trans('language.orders_add_new')), URL::to('dashboard/orders/create'));
});

// Home > Orders > Edit
Breadcrumbs::register('orders_edit', function($breadcrumbs, $order)
{
    $breadcrumbs->parent('orders');
    $breadcrumbs->push(e(trans('language.orders_order')) . ' #' . $order['id'], URL::to('dashboard/orders/edit/' . $order['id']));
});

// Home > Customers
Breadcrumbs::register('customers', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push(e(trans('language.sidebar_customers')), URL::to('dashboard/customers'));
});

// Home > Customers > Create
Breadcrumbs::register('customers_create', function($breadcrumbs)
{
    $breadcrumbs->parent('customers');
    $breadcrumbs->push(e(trans('language.customers_add_new')), URL::to('dashboard/customers/create'));
});

// Home > Customers > Edit
Breadcrumbs::register('customers_edit', function($breadcrumbs, $customer)
{
    $breadcrumbs->parent('customers');
    $breadcrumbs->push($customer['first_name'] . ' ' . $customer['last_name'], URL::to('dashboard/customers/edit/' . $customer['id']));
});

// Home > Products
Breadcrumbs::register('products', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push(e(trans('language.sidebar_products')), URL::to('dashboard/products'));
});

// Home > Products > Create
Breadcrumbs::register('products_create', function($breadcrumbs)
{
    $breadcrumbs->parent('products');
    $breadcrumbs->push(e(trans('language.products_add_new')), URL::to('dashboard/products/create'));
});

// Home > Products > Edit
Breadcrumbs::register('products_edit', function($breadcrumbs, $product)
{
    $breadcrumbs->parent('products');
    $breadcrumbs->push($product['sku'], URL::to('dashboard/products/edit/' . $product['id']));
});

// Home > Transport Companies
Breadcrumbs::register('transports', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push(e(trans('language.sidebar_transports')), URL::to('dashboard/transports'));
});

// Home > Transport Companies > Create
Breadcrumbs::register('transports_create', function($breadcrumbs)
{
    $breadcrumbs->parent('transports');
    $breadcrumbs->push(e(trans('language.transports_add_new')), URL::to('dashboard/transports/create'));
});

// Home > Transport Companies > Edit
Breadcrumbs::register('transports_edit', function($breadcrumbs, $transport)
{
    $breadcrumbs->parent('transports');
    $breadcrumbs->push($transport['name'], URL::to('dashboard/transports/edit/' . $transport['id']));
});

// Home > Profile
Breadcrumbs::register('profile', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push(e(trans('language.sidebar_profile')), URL::to('dashboard/profile'));
});

// Home > Access Logs
Breadcrumbs::register('accessLogs', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push(e(trans('language.sidebar_access')), URL::to('dashboard/logs/access'));
});

// Home > Error Logs
Breadcrumbs::register('errorLogs', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push(e(trans('language.sidebar_error')), URL::to('dashboard/logs/errors'));
});

// Home > Error Logs > Error #
Breadcrumbs::register('errorLogs_details', function($breadcrumbs, $error)
{
    $breadcrumbs->parent('errorLogs');
    $breadcrumbs->push(e(trans('language.errorlogs_error')) . ' #' . $error['id'], URL::to('dashboard/logs/errors/details/' . $error['id']));
});