<?php namespace App\Http\Controllers;


use App\Http\Controllers\Auth\PasswordController;
use App\User;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ExtendedPasswordController extends PasswordController
{
    public function getEmail()
    {
        $validator = \JsValidator::make(array('email' => 'required|email'));
        return view('auth.password')->with('validator', $validator);
    }

    public function getReset($token = null)
    {
        if (is_null($token))
        {
            throw new NotFoundHttpException;
        }

        $email = User::getInfoByResetToken($token);
        $validator = \JsValidator::make(array(
            'email' => 'required|email',
            'password' => 'required|confirmed',
            'password_confirmation' => 'required|same:password',
        ));
        return view('auth.reset')->with('token', $token)->with('email', $email->email)->with('validator', $validator);
    }
}