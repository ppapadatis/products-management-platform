<?php namespace App\Http\Controllers;


use App\Http\Controllers\Auth\AuthController;

class ExtendedAuthController extends AuthController
{
    public function getLogin()
    {
        $validator = \JsValidator::make(array('email' => 'required|email', 'password' => 'required'));
        return view('auth.login')->with('validator', $validator);
    }
}