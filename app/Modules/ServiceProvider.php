<?php namespace App\Modules;


class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function boot()
    {
        $config = config("modules.Modules");
        $modules = array_keys($config);
        while (list(,$module) = each($modules)) {
            if (file_exists(app_path('Modules/' . $module . '/routes.php'))) {
                include app_path('Modules/' . $module . '/routes.php');
            }
            while (list(, $controller) = each($config[$module])) {
                if (is_dir(app_path('Modules/' . $module . '/Views/' . $controller))) {
                    $this->loadViewsFrom(app_path('Modules/' . $module . '/Views/' . $controller), $module . '.' . $controller);
                }
            }
        }
    }

    public function register() {}
}