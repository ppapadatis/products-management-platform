<?php namespace App\Modules\Core\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Modules\Core\Models\Product;

class ProductsController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Response
	 */
	public function index()
	{
        $products = Product::all();
		return \View::make('Core.Products::index', compact('products'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Response
	 */
	public function create()
	{
        $validator = \JsValidator::make(Product::$rules);
        return \View::make('Core.Products::create', compact('validator'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return \Response
	 */
	public function store()
	{
        $data = \Input::all();

        $productValidator = \Validator::make($data, Product::$rules);

        if ($productValidator->fails())
        {
            \Session::flash('notification_create_fail', e(trans('language.notification_create_fail')));
            return \Redirect::back()->withErrors($productValidator)->withInput($data);
        }

        Product::create($data);

        \Session::flash('notification_create_success', e(trans('language.notification_create_success')));
        return \Redirect::to('/dashboard/products');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Response
	 */
	public function edit($id)
	{
        $validator = \JsValidator::make(Product::$rules);
        $product = Product::find($id);

        if (!$product) {
            return \View::make('Core.Products::edit', array('not_found' => true));
        }

        return \View::make('Core.Products::edit', compact('product', 'validator'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return \Response
	 */
	public function update($id)
	{
        $product = Product::find($id);

        if (!$product) {
            \Session::flash('notification_update_fail', e(trans('language.notification_update_fail')));
            return \Redirect::to('/dashboard/products');
        }

        $data = \Input::all();

        $productRules = Product::$rules;
        $productValidator = \Validator::make($data, $productRules);

        if ($productValidator->fails())
        {
            \Session::flash('notification_update_fail', e(trans('language.notification_update_fail')));
            return \Redirect::back()->withErrors($productValidator)->withInput($data);
        }

        $product->update($data);

        \Session::flash('notification_update_success', e(trans('language.notification_update_success')));
        return \Redirect::to('/dashboard/products');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
	public function destroy($id)
	{
        $product = Product::find($id);

        if (!$product) {
            \Session::flash('notification_delete_fail', e(trans('language.notification_delete_fail')));
            return response(array('message' => 'error'), 404, array('Content-type' => 'application/json'));
        }

        $product->delete();

        \Session::flash('notification_delete_success', e(trans('language.notification_delete_success')));
        return response(array('message' => 'success'), 200, array('Content-type' => 'application/json'));
	}

}
