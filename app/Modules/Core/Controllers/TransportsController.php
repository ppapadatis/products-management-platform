<?php namespace App\Modules\Core\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Modules\Core\Models\Address;
use App\Modules\Core\Models\TransportCompany;

class TransportsController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Response
     */
    public function index()
    {
        $transports = TransportCompany::all();
        return \View::make('Core.Transports::index', compact('transports'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Response
     */
    public function create()
    {
        $validator = \JsValidator::make(array_merge(TransportCompany::$rules, Address::$rules));
        return \View::make('Core.Transports::create', compact('validator'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Response
     */
    public function store()
    {
        $data = \Input::all();

        $transportValidator = \Validator::make($data, TransportCompany::$rules);
        $addressValidator = \Validator::make($data['address'], Address::$rules);

        if ($transportValidator->fails() || $addressValidator->fails())
        {
            \Session::flash('notification_create_fail', e(trans('language.notification_create_fail')));
            return \Redirect::back()->withErrors(array_merge_recursive(
                $transportValidator->messages()->toArray(), $addressValidator->messages()->toArray()))->withInput($data);
        }

        $transport = TransportCompany::create($data);
        $address = new Address($data['address']);
        $transport->address()->save($address);

        \Session::flash('notification_create_success', e(trans('language.notification_create_success')));
        return \Redirect::to('/dashboard/transports');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Response
     */
    public function edit($id)
    {
        $validator = \JsValidator::make(array_merge(TransportCompany::$rules, Address::$rules));
        $transport = TransportCompany::with('address')->find($id);

        if (!$transport) {
            return \View::make('Core.Transports::edit', array('not_found' => true));
        }

        return \View::make('Core.Transports::edit', compact('transport', 'validator'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return \Response
     */
    public function update($id)
    {
        $transport = TransportCompany::with('address')->find($id);

        if (!$transport) {
            \Session::flash('notification_update_fail', e(trans('language.notification_update_fail')));
            return \Redirect::to('/dashboard/transports');
        }

        $data = \Input::all();

        $transportRules = TransportCompany::$rules;
        $transportValidator = \Validator::make($data, $transportRules);
        $addressValidator = \Validator::make($data['address'], Address::$rules);

        if ($transportValidator->fails() || $addressValidator->fails())
        {
            \Session::flash('notification_update_fail', e(trans('language.notification_update_fail')));
            return \Redirect::back()->withErrors(array_merge_recursive(
                $transportValidator->messages()->toArray(), $addressValidator->messages()->toArray()))->withInput($data);
        }

        $transport->update($data);
        $transport->address()->update($data['address']);

        \Session::flash('notification_update_success', e(trans('language.notification_update_success')));
        return \Redirect::to('/dashboard/transports');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function destroy($id)
    {
        $transport = TransportCompany::find($id);

        if (!$transport) {
            \Session::flash('notification_delete_fail', e(trans('language.notification_delete_fail')));
            return response(array('message' => 'error'), 404, array('Content-type' => 'application/json'));
        }

        $transport->address()->delete();
        $transport->delete();

        \Session::flash('notification_delete_success', e(trans('language.notification_delete_success')));
        return response(array('message' => 'success'), 200, array('Content-type' => 'application/json'));
    }

}
