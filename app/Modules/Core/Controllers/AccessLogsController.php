<?php namespace App\Modules\Core\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Modules\Core\Models\AccessLog;

class AccessLogsController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Response
	 */
	public function index()
	{
        $accessLogs = AccessLog::all()->sortBy('created_at', SORT_REGULAR, true)->take(50);
		return \View::make('Core.AccessLogs::index', compact('accessLogs'));
	}

}
