<?php namespace App\Modules\Core\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Modules\Core\Models\Customer;
use App\Modules\Core\Models\Order;
use App\Modules\Core\Models\OrderItem;
use App\Modules\Core\Models\Product;
use App\Modules\Core\Models\Size;

class OrdersController extends Controller {

    public $customerArray = array();
    public $productArray = array();
    public $sizeArray = array();

    public function __construct()
    {
        $customers = Customer::all();
        $this->customerArray = array(0 => null);
        foreach ($customers as $customer) {
            $this->customerArray[$customer['id']] = $customer['first_name'] . ' ' . $customer['last_name'] . ' (' . $customer['afm'] . ')';
        }

        $products = Product::all();
        $this->productArray = array(0 => null);
        foreach ($products as $product) {
            $this->productArray[$product['id']] = $product['sku'];
        }

        $sizes = Size::all();
        $this->sizeArray = array(0 => null);
        foreach ($sizes as $size) {
            $this->sizeArray[$size['id']] = $size['name'];
        }
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Response
	 */
	public function index()
	{
        $orders = Order::all()->sortBy('created_at', SORT_REGULAR, true);
		return \View::make('Core.Orders::index', compact('orders'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Response
	 */
	public function create()
	{
        $validator = \JsValidator::make(array_merge(Order::$rules, array(
            'product1' => 'required',
            'quantity1' => 'required|integer',
            'size1' => 'required',
        )));

        return \View::make('Core.Orders::create', array(
            'customerArray' => $this->customerArray,
            'productArray' => $this->productArray,
            'sizeArray' => $this->sizeArray,
            'validator' => $validator
        ));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return \Response
	 */
	public function store()
	{
        $data = \Input::all();

        $orderArray = array(
            'customer_id' => $data['customer_id'] > 0 ? $data['customer_id'] : null,
            'discount' => $data['discount'],
            'vat' => $data['vat'],
        );

        $orderValidator = \Validator::make($orderArray, Order::$rules);

        if ($orderValidator->fails())
        {
            \Session::flash('notification_create_fail', e(trans('language.notification_create_fail')));
            return \Redirect::back()->withErrors($orderValidator)->withInput($orderArray);
        }

        $inputsArray = $this->_inputsToArray($data);

        foreach ($inputsArray as $inputArray) {
            $orderItemValidator = \Validator::make($inputArray, OrderItem::$rules);

            if ($orderItemValidator->fails())
            {
                \Session::flash('notification_create_fail', e(trans('language.notification_create_fail')));
                return \Redirect::back()->withErrors($orderItemValidator)->withInput($orderArray);
            }
        }

        $order = Order::create($orderArray);
        foreach ($inputsArray as $inputArray) {
            $inputArray['order_id'] = $order['id'];
            OrderItem::create($inputArray);
        }

        \Session::flash('notification_create_success', e(trans('language.notification_create_success')));
        return \Redirect::to('/dashboard/orders');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Response
	 */
	public function edit($id)
	{
        $validator = \JsValidator::make(array_merge(Order::$rules, array(
            'product[]' => 'required',
            'quantity[]' => 'required|integer',
            'size[]' => 'required',
        )));

        $order = Order::with(array('customer', 'orderItems'))->find($id);

        if (!$order) {
            return \View::make('Core.Orders::edit', array('not_found' => true));
        }

        return \View::make('Core.Orders::edit', array(
            'order' => $order,
            'customerArray' => $this->customerArray,
            'productArray' => $this->productArray,
            'sizeArray' => $this->sizeArray,
            'validator' => $validator
        ));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return \Response
	 */
	public function update($id)
	{
        $order = Order::with(array('customer', 'orderItems'))->find($id);

        if (!$order) {
            \Session::flash('notification_update_fail', e(trans('language.notification_update_fail')));
            return \Redirect::to('/dashboard/orders');
        }

        $data = \Input::all();

        $orderArray = array(
            'customer_id' => $data['customer_id'] > 0 ? $data['customer_id'] : 0,
            'discount' => $data['discount'],
            'vat' => $data['vat'],
        );

        $orderValidator = \Validator::make($orderArray, Order::$rules);

        if ($orderValidator->fails())
        {
            \Session::flash('notification_create_fail', e(trans('language.notification_create_fail')));
            return \Redirect::back()->withErrors($orderValidator)->withInput($orderArray);
        }

        $inputsArray = $this->_inputsToArray($data);

        foreach ($inputsArray as $inputArray) {
            $orderItemValidator = \Validator::make($inputArray, OrderItem::$rules);

            if ($orderItemValidator->fails())
            {
                \Session::flash('notification_create_fail', e(trans('language.notification_create_fail')));
                return \Redirect::back()->withErrors($orderItemValidator)->withInput($orderArray);
            }
        }

        $order->update($orderArray);

        // Remove all products from input.
        $order->orderItems()->delete();

        // Add all products from input.
        foreach ($inputsArray as $inputArray) {
            $inputArray['order_id'] = $order['id'];
            OrderItem::create($inputArray);
        }

        \Session::flash('notification_update_success', e(trans('language.notification_update_success')));
        return \Redirect::to('/dashboard/orders');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
	public function destroy($id)
	{
        $order = Order::find($id);

        if (!$order) {
            \Session::flash('notification_delete_fail', e(trans('language.notification_delete_fail')));
            return response(array('message' => 'error'), 404, array('Content-type' => 'application/json'));
        }

        $order->orderItems()->delete();
        $order->delete();

        \Session::flash('notification_delete_success', e(trans('language.notification_delete_success')));
        return response(array('message' => 'success'), 200, array('Content-type' => 'application/json'));
	}

    private function _inputsToArray($data)
    {
        $dataArray = array();

        $total = (count($data) - 4) / 5;

        for ($i = 1; $i <= $total; $i++) {
            $dataArray[] = array(
                'product_id' => $data['product' . $i] > 0 ? $data['product' . $i] : null,
                'quantity' => $data['quantity' . $i],
                'color' => $data['color' . $i],
                'size_id' => $data['size' . $i] > 0 ? $data['size' . $i] : null,
                'notes' => $data['notes' . $i]
            );
        }

        return $dataArray;
    }

}
