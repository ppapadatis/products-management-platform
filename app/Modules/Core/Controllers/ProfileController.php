<?php namespace App\Modules\Core\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;

class ProfileController extends Controller {

    public $rules = array(
        'name' => 'required',
        'email' => 'required|email',
        'password' => 'required|confirmed',
        'password_confirmation' => 'required|same:password'
    );

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Response
	 */
	public function index()
	{
        $validator = \JsValidator::make($this->rules);
        $user = \Auth::getUser();
		return \View::make('Core.Profile::index', compact('user', 'validator'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @return \Response
	 */
	public function update()
	{
        $user = \Auth::getUser();
        $data = \Input::all();

        $userValidator = \Validator::make($data, $this->rules);

        if ($userValidator->fails())
        {
            \Session::flash('notification_update_fail', e(trans('language.notification_update_fail')));
            return \Redirect::back()->withErrors($userValidator)->withInput($data);
        }

        $data['password'] = \Hash::make($data['password']);

        $user->update($data);
        \Auth::login($user);

        \Session::flash('notification_update_success', e(trans('language.notification_update_success')));
        return \Redirect::to('/dashboard/profile');
	}

}
