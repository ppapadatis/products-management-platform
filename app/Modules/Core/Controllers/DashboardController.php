<?php namespace App\Modules\Core\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Modules\Core\Models\Customer;
use App\Modules\Core\Models\Order;
use App\Modules\Core\Models\Product;
use App\Modules\Core\Models\TransportCompany;
use Illuminate\Http\Request;

class DashboardController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Response
	 */
	public function index()
	{
        $orders = Order::all()->count();
        $customers = Customer::all()->count();
        $products = Product::all()->count();
        $transports = TransportCompany::all()->count();
        $latestOrders = Order::all()->sortBy('created_at', SORT_REGULAR, true)->take(7);
        $latestProducts = Product::all()->sortBy('created_at', SORT_REGULAR, true)->take(4);
		return \View::make('Core.Dashboard::index', compact('orders', 'customers', 'products', 'transports', 'latestOrders', 'latestProducts'));
	}

}
