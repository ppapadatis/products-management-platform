<?php namespace App\Modules\Core\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Modules\Core\Models\Address;
use App\Modules\Core\Models\Customer;
use App\Modules\Core\Models\TransportCompany;

class CustomersController extends Controller {

    public $transportArray;

    public function __construct()
    {
        $transports = TransportCompany::all();

        $this->transportArray[0] = null;
        foreach ($transports as $transport) {
            $this->transportArray[$transport['id']] = $transport['name'];
        }
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Response
	 */
	public function index()
	{
        $customers = Customer::all();
		return \View::make('Core.Customers::index', compact('customers'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Response
	 */
	public function create()
	{
        $validator = \JsValidator::make(array_merge(Customer::$rules, Address::$rules));
        return \View::make('Core.Customers::create', compact('validator'))->with('transportArray', $this->transportArray);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return \Response
	 */
	public function store()
	{
        $data = \Input::all();

        $customerValidator = \Validator::make($data, Customer::$rules);
        $addressValidator = \Validator::make($data['address'], Address::$rules);

        if ($customerValidator->fails() || $addressValidator->fails())
        {
            \Session::flash('notification_create_fail', e(trans('language.notification_create_fail')));
            return \Redirect::back()->withErrors(array_merge_recursive(
                $customerValidator->messages()->toArray(), $addressValidator->messages()->toArray()))->withInput($data);
        }

        if ($data['transport_id'] == 0) $data['transport_id'] = null;

        $customer = Customer::create($data);
        $address = new Address($data['address']);
        $customer->address()->save($address);

        \Session::flash('notification_create_success', e(trans('language.notification_create_success')));
        return \Redirect::to('/dashboard/customers');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Response
	 */
	public function edit($id)
	{
        $validator = \JsValidator::make(array_merge(Customer::$rules, Address::$rules));
        $customer = Customer::with('address')->find($id);

        if (!$customer) {
            return \View::make('Core.Customers::edit', array('not_found' => true));
        }

        return \View::make('Core.Customers::edit', compact('customer', 'validator'))->with('transportArray', $this->transportArray);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return \Response
	 */
	public function update($id)
	{
        $customer = Customer::with('address')->find($id);

        if (!$customer) {
            \Session::flash('notification_update_fail', e(trans('language.notification_update_fail')));
            return \Redirect::to('/dashboard/customers');
        }

        $data = \Input::all();

        $customerRules = Customer::$rules;
        $customerValidator = \Validator::make($data, $customerRules);
        $addressValidator = \Validator::make($data['address'], Address::$rules);

        if ($customerValidator->fails() || $addressValidator->fails())
        {
            \Session::flash('notification_update_fail', e(trans('language.notification_update_fail')));
            return \Redirect::back()->withErrors(array_merge_recursive(
                $customerValidator->messages()->toArray(), $addressValidator->messages()->toArray()))->withInput($data);
        }

        if ($data['transport_id'] == 0) $data['transport_id'] = null;

        $customer->update($data);
        $customer->address()->update($data['address']);

        \Session::flash('notification_update_success', e(trans('language.notification_update_success')));
        return \Redirect::to('/dashboard/customers');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
	public function destroy($id)
	{
        $customer = Customer::find($id);

        if (!$customer) {
            \Session::flash('notification_delete_fail', e(trans('language.notification_delete_fail')));
            return response(array('message' => 'error'), 404, array('Content-type' => 'application/json'));
        }

        $customer->address()->delete();
        $customer->delete();

        \Session::flash('notification_delete_success', e(trans('language.notification_delete_success')));
        return response(array('message' => 'success'), 200, array('Content-type' => 'application/json'));
	}

}
