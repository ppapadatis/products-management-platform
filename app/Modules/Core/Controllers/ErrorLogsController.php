<?php namespace App\Modules\Core\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Modules\Core\Models\ErrorLog;

class ErrorLogsController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Response
	 */
	public function index()
	{
        $errorLogs = ErrorLog::all()->sortBy('created_at', SORT_REGULAR, true)->take(50);

        foreach ($errorLogs as $errorLog) {
            $date = date_format($errorLog['created_at'], 'Y,m,d');
            list($year, $month, $date) = explode(',', $date);
            $logs[$date . '/' . $month . '/' . $year][] = $errorLog;
        }

		return \View::make('Core.ErrorLogs::index', compact('logs'));
	}

    /**
     * Show the details of the specified resource.
     *
     * @param  int  $id
     * @return \Response
     */
    public function details($id)
    {
        $errorlog = ErrorLog::find($id);

        if (!$errorlog) {
            return \View::make('Core.ErrorLogs::details', array('not_found' => true));
        }

        return \View::make('Core.ErrorLogs::details', compact('errorlog'));
    }
}
