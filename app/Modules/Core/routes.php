<?php

/** CSRF Filter */
Route::filter('csrf', function(){
    $token = Request::ajax() ? Request::header('x-csrf-token') : Input::get('_token');

    if (Session::token() != $token) {
        throw new \Illuminate\Session\TokenMismatchException;
    }
});

/** Route Group with Auth Middleware */
Route::group(array('middleware' => 'auth', 'module' => 'Core', 'namespace' => 'App\Modules\Core\Controllers'), function() {

    /** ID Pattern */
    Route::pattern('id', '[0-9]+');

    /** Orders Routes */
    Route::get('dashboard/orders', 'OrdersController@index');
    Route::get('dashboard/orders/create', 'OrdersController@create');
    Route::post('dashboard/orders/store', 'OrdersController@store');
    Route::get('dashboard/orders/edit/{id}', 'OrdersController@edit');
    Route::put('dashboard/orders/update/{id}', 'OrdersController@update');
    Route::delete('dashboard/orders/delete/{id}', 'OrdersController@destroy');

    /** Customers Routes */
    Route::get('dashboard/customers', 'CustomersController@index');
    Route::get('dashboard/customers/create', 'CustomersController@create');
    Route::post('dashboard/customers/store', 'CustomersController@store');
    Route::get('dashboard/customers/edit/{id}', 'CustomersController@edit');
    Route::put('dashboard/customers/update/{id}', 'CustomersController@update');
    Route::delete('dashboard/customers/delete/{id}', 'CustomersController@destroy');

    /** Products Routes */
    Route::get('dashboard/products', 'ProductsController@index');
    Route::get('dashboard/products/create', 'ProductsController@create');
    Route::post('dashboard/products/store', 'ProductsController@store');
    Route::get('dashboard/products/edit/{id}', 'ProductsController@edit');
    Route::put('dashboard/products/update/{id}', 'ProductsController@update');
    Route::delete('dashboard/products/delete/{id}', 'ProductsController@destroy');

    /** Transport Companies Routes */
    Route::get('dashboard/transports', 'TransportsController@index');
    Route::get('dashboard/transports/create', 'TransportsController@create');
    Route::post('dashboard/transports/store', 'TransportsController@store');
    Route::get('dashboard/transports/edit/{id}', 'TransportsController@edit');
    Route::put('dashboard/transports/update/{id}', 'TransportsController@update');
    Route::delete('dashboard/transports/delete/{id}', 'TransportsController@destroy');

    /** Profile Routes */
    Route::get('dashboard/profile', 'ProfileController@index');
    Route::put('dashboard/profile/update', 'ProfileController@update');

    /** Access Logs Routes */
    Route::get('dashboard/logs/access', 'AccessLogsController@index');

    /** Error Logs Routes */
    Route::get('dashboard/logs/errors', 'ErrorLogsController@index');
    Route::get('dashboard/logs/errors/details/{id}', 'ErrorLogsController@details');

    /** Dashboard */
    Route::get('dashboard', 'DashboardController@index');

});