@extends('app')

@section('page-title', e(trans('language.main_title') . ': ' . trans('language.errorlogs_title')))

@section('header')
<h1>{{ e(trans('language.errorlogs_title')) }}</h1>
{!! Breadcrumbs::render('errorLogs') !!}
@endsection

{{-- Main html --}}
@section('content')
<div class="row">
    <div class="col-lg-12">
    @if (isset($logs) && !empty($logs))
        <ul class="timeline">
            @foreach ($logs as $logDate => $logArray)
                <li class="time-label">
                    <span class="bg-red">{{ $logDate }}</span>
                </li>
                @foreach ($logArray as $singleLog)
                    <li>
                        <i class="fa fa-warning bg-yellow"></i>
                        <div class="timeline-item">
                            <span class="time"><i class="fa fa-clock-o"></i> {{ date_format($singleLog['created_at'], 'H:i:s') }}</span>
                            <h3 class="timeline-header"><a href="javascript:void(0)" style="cursor: default">{{ e(trans('language.errorlogs_severity')) }}</a>: {{ $singleLog['level'] }}</h3>
                            <div class="timeline-body">{{ $singleLog['details'] }}</div>
                            <div class="timeline-footer">
                                <a class="btn btn-primary btn-xs" href="{{ url('/dashboard/logs/errors/details/' . $singleLog['id']) }}">{{ e(trans('language.errorlogs_read_more')) }}</a>
                            </div>
                        </div>
                    </li>
                @endforeach
            @endforeach
        </ul>
    @else
        <div class="alert alert-info">
            <h4><i class="icon fa fa-info"></i></h4>
            {{ e(trans('language.errorlogs_empty_warning')) }}.
        </div>
    @endif
    </div>
</div>
@endsection