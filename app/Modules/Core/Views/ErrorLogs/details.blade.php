@extends('app')

@section('page-title', e(trans('language.main_title') . ': ' . trans('language.errorlogs_details')))

@section('header')
<h1>{{ e(trans('language.errorlogs_details')) }}</h1>
{!! Breadcrumbs::render('errorLogs_details', $errorlog) !!}
@endsection

{{-- Main html --}}
@section('content')
<div class="row">
    <div class="col-lg-12">
    @if (isset($not_found))
        <div class="alert alert-info">
            <h4><i class="icon fa fa-info"></i></h4>
            {{ e(trans('language.errorlogs_empty_warning')) }}.
        </div>
        <a href="{{ url('/dashboard/logs/errors') }}" class="btn btn-primary">{{ e(trans('language.form_return')) }}</a>
    @else
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">{{ e(trans('language.errorlogs_id')) }}: {{ e($errorlog['id']) }}</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-12">
                        <h4>{{ e(trans('language.errorlogs_severity')) }}:</h4>
                        {{ e($errorlog['level']) }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <h4>{{ e(trans('language.errorlogs_details')) }}:</h4>
                        {{ e($errorlog['details']) }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <h4>{{ e(trans('language.errorlogs_user_agent')) }}:</h4>
                        {{ e($errorlog['user_agent']) }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <h4>{{ e(trans('language.errorlogs_remote_ip')) }}:</h4>
                        {{ e($errorlog['remote_ip']) }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <h4>{{ e(trans('language.errorlogs_user')) }}:</h4>
                        {{ !is_null($errorlog['user']) ? e($errorlog['user']['name']) : e('-') }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <h4>{{ e(trans('language.errorlogs_stacktrace')) }}:</h4>
                        {{ e($errorlog['message']) }}
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="pull-left">
                            <a href="{{ url('/dashboard/logs/errors') }}" class="btn btn-primary">{{ e(trans('language.form_return')) }}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    </div>
</div>
@endsection