@extends('app')

@section('page-title', e(trans('language.main_title') . ': ' . trans('language.accesslogs_title')))

@section('header')
<h1>{{ e(trans('language.accesslogs_title')) }}</h1>
{!! Breadcrumbs::render('accessLogs') !!}
@endsection

{{-- Main html --}}
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="box box-primary">
            <div class="box-body">
                @if (!$accessLogs->isEmpty())
                    <table class="table table-striped table-bordered table-hover" id="dataTables-customers">
                        <thead>
                        <tr>
                            <th>{{ e(trans('language.accesslogs_remote_ip')) }}</th>
                            <th>{{ e(trans('language.accesslogs_user')) }}</th>
                            <th>{{ e(trans('language.accesslogs_date')) }}</th>
                            <th>{{ e(trans('language.accesslogs_status')) }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $odd = true ?>
                        @foreach ($accessLogs as $accessLog)
                            @if ($odd)
                                <?php $class = 'odd' ?>
                            @else
                                <?php $class = 'even' ?>
                            @endif
                            <?php $odd = !$odd ?>
                            <tr class="{{ $class }}">
                                <td>{{ e($accessLog['remote_ip']) }}</td>
                                <td>{{ !is_null($accessLog['user']) ? e($accessLog['user']['name']) : e('-') }}</td>
                                <td><i class="fa fa-clock-o"></i> {{ e(date_format($accessLog['created_at'], 'H:i:s d/m/Y')) }}</td>
                                <td>
                                    @if ($accessLog['success'])
                                        <span class="label label-success">{{ e(trans('language.accesslogs_status_ok')) }}</span></td>
                                    @else
                                        <span class="label label-danger">{{ e(trans('language.accesslogs_status_not_ok')) }}</span></td>
                                    @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @else
                    <div class="alert alert-info">
                        <h4><i class="icon fa fa-info"></i></h4>
                        {{ e(trans('language.accesslogs_empty_warning')) }}.
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection