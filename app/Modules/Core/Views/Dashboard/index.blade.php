@extends('app')

@section('page-title', e(trans('language.main_title') . ': ' . trans('language.sidebar_dashboard')))

@section('header')
<h1>{{ e(trans('language.sidebar_dashboard')) }}</h1>
{!! Breadcrumbs::render('home') !!}
@endsection

{{-- Main html --}}
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title"></h3>
            </div>
            <div class="box-body">
                <!-- Small boxes (Stat box) -->
                <div class="row">
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-aqua">
                            <div class="inner">
                                <h3>{{ $orders }}</h3>
                                <p>{{ e(trans('language.widget_orders')) }}</p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-shopping-cart"></i>
                            </div>
                            <a href="{{ url('/dashboard/orders') }}" class="small-box-footer">{{ e(trans('language.modal_more_info')) }} <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div><!-- ./col -->
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-yellow">
                            <div class="inner">
                                <h3>{{ $customers }}</h3>
                                <p>{{ e(trans('language.widget_customers')) }}</p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-users"></i>
                            </div>
                            <a href="{{ url('/dashboard/customers') }}" class="small-box-footer">{{ e(trans('language.modal_more_info')) }} <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div><!-- ./col -->
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-green">
                            <div class="inner">
                                <h3>{{ $products }}</h3>
                                <p>{{ e(trans('language.widget_products')) }}</p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-cubes"></i>
                            </div>
                            <a href="{{ url('/dashboard/products') }}" class="small-box-footer">{{ e(trans('language.modal_more_info')) }} <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div><!-- ./col -->
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-red">
                            <div class="inner">
                                <h3>{{ $transports }}</h3>
                                <p>{{ e(trans('language.widget_transports')) }}</p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-truck"></i>
                            </div>
                            <a href="{{ url('/dashboard/transports') }}" class="small-box-footer">{{ e(trans('language.modal_more_info')) }} <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div><!-- ./col -->
                </div><!-- /.row -->
                <div class="row">
                    <div class="col-lg-8">
                        <!-- TABLE: LATEST ORDERS -->
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title">{{ e(trans('language.widget_latest_orders')) }}</h3>
                            </div><!-- /.box-header -->
                            <div class="box-body">
                                <div class="table-responsive">
                                    <table class="table no-margin">
                                        <thead>
                                        <tr>
                                            <th>{{ e(trans('language.orders_customer')) }}</th>
                                            <th>{{ e(trans('language.orders_discount')) }}</th>
                                            <th>{{ e(trans('language.orders_vat')) }}</th>
                                            <th>{{ e(trans('language.orders_date')) }}</th>
                                            <th class="nosort" style="width: 43px"></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if ($latestOrders->isEmpty())
                                            <tr>
                                                <td colspan="5">
                                                    <div class="alert alert-info">
                                                        <h4><i class="icon fa fa-info"></i></h4>
                                                        {{ e(trans('language.orders_empty_warning')) }}.
                                                    </div>
                                                </td>
                                            </tr>
                                        @else
                                            @foreach ($latestOrders as $latestOrder)
                                                <tr>
                                                    <td>{{ e($latestOrder['customer']['first_name'] . ' ' .  $latestOrder['customer']['last_name']) }}</td>
                                                    <td>{{ e($latestOrder['discount'] . ' %') }}</td>
                                                    <td>{{ e($latestOrder['vat'] . ' %') }}</td>
                                                    <td><i class="fa fa-clock-o"></i> {{ $latestOrder['created_at'] }}</td>
                                                    <td>
                                                        <div class="tooltip-buttons">
                                                            <a href="{{ url('/dashboard/orders/edit/' . $latestOrder['id']) }}" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="{{ e(trans('language.form_view')) }}"><i class="fa fa-eye"></i></a>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div><!-- /.table-responsive -->
                            </div><!-- /.box-body -->
                            <div class="box-footer clearfix">
                                <a href="{{ url('/dashboard/orders/create') }}" class="btn btn-sm btn-info btn-flat pull-left">{{ e(trans('language.widget_place_order')) }}</a>
                                <a href="{{ url('/dashboard/orders') }}" class="btn btn-sm btn-default btn-flat pull-right">{{ e(trans('language.widget_view_orders')) }}</a>
                            </div><!-- /.box-footer -->
                        </div><!-- /.box -->
                    </div><!-- /.col -->
                    <div class="col-lg-4">
                        <!-- PRODUCT LIST -->
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">{{ e(trans('language.widget_latest_products')) }}</h3>
                            </div><!-- /.box-header -->
                            <div class="box-body">
                                <ul class="products-list product-list-in-box">
                                    @if ($latestProducts->isEmpty())
                                        <li class="item">
                                            <div class="alert alert-info">
                                                <h4><i class="icon fa fa-info"></i></h4>
                                                {{ e(trans('language.products_empty_warning')) }}.
                                            </div>
                                        </li><!-- /.item -->
                                    @else
                                        @foreach ($latestProducts as $latestProduct)
                                            <li class="item">
                                                <a href="{{ url('/dashboard/products/edit/' . e($latestProduct['id'])) }}" class="product-title">{{ e($latestProduct['sku']) }} <span class="label label-info pull-right">{{ e('€ ') . e($latestProduct['wholesale_price']) }}</span></a>
                                                <span class="product-description">{{ e($latestProduct['description']) }}</span>
                                            </li><!-- /.item -->
                                        @endforeach
                                    @endif
                                </ul>
                            </div><!-- /.box-body -->
                            <div class="box-footer text-center">
                                <a href="{{ url('/dashboard/products') }}" class="uppercase">{{ e(trans('language.widget_view_products')) }}</a>
                            </div><!-- /.box-footer -->
                        </div><!-- /.box -->
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div>
            <!-- /.panel-body -->
        </div>
    </div>
</div>
@endsection