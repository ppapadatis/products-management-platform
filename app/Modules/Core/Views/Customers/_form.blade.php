<div class="row">
    <div class="col-lg-4">
        <div class="form-group @if ($errors->has('first_name')) has-error @endif">
            {!! Form::label('first_name', e(trans("language.customers_fname")), array('class' => 'control-label')) !!}
            {!! Form::text('first_name', Input::old('first_name'), array('class' => 'form-control', 'placeholder' => e(trans('language.customers_fname')), 'autofocus' => 'autofocus', 'required' => 'required')) !!}
            @if ($errors->has('first_name')) <p class="help-block">{{ e($errors->first('first_name')) }}</p> @endif
        </div>
    </div>
    <!-- /.col-lg-4 -->
    <div class="col-lg-4">
        <div class="form-group @if ($errors->has('last_name')) has-error @endif">
            {!! Form::label('last_name', e(trans("language.customers_lname")), array('class' => 'control-label')) !!}
            {!! Form::text('last_name', Input::old('last_name'), array('class' => 'form-control', 'placeholder' => e(trans('language.customers_lname')), 'required' => 'required')) !!}
            @if ($errors->has('last_name')) <p class="help-block">{{ e($errors->first('last_name')) }}</p> @endif
        </div>
    </div>
    <!-- /.col-lg-4 -->
    <div class="col-lg-4">
        <div class="form-group @if ($errors->has('distinctive_title')) has-error @endif">
            {!! Form::label('distinctive_title', e(trans("language.customers_distTitle")), array('class' => 'control-label')) !!} <em>({{ e(trans('language.form_optional')) }})</em>
            {!! Form::text('distinctive_title', Input::old('distinctive_title'), array('class' => 'form-control', 'placeholder' => e(trans('language.customers_distTitle')))) !!}
            @if ($errors->has('distinctive_title')) <p class="help-block">{{ e($errors->first('distinctive_title')) }}</p> @endif
        </div>
    </div>
    <!-- /.col-lg-4 -->
</div>
<div class="row">
    <div class="col-lg-4">
        <div class="form-group @if ($errors->has('transport_id')) has-error @endif">
            {!! Form::label('transport_id', e(trans("language.customers_transport")), array('class' => 'control-label')) !!} <em>({{ e(trans('language.form_optional')) }})</em>
            {!! Form::select('transport_id', $transportArray, Input::old('transport_id'), array('class' => 'form-control chosen', 'placeholder' => e(trans('language.customers_transport')))) !!}
            @if ($errors->has('transport_id')) <p class="help-block">{{ e($errors->first('transport_id')) }}</p> @endif
        </div>
    </div>
    <!-- /.col-lg-4 -->
    <div class="col-lg-4">
        <div class="form-group @if ($errors->has('afm')) has-error @endif">
            {!! Form::label('afm', e(trans("language.customers_afm")), array('class' => 'control-label')) !!}
            {!! Form::input('number', 'afm', Input::old('afm'), array('class' => 'form-control', 'placeholder' => e(trans('language.customers_afm')), 'required' => 'required')) !!}
            @if ($errors->has('afm')) <p class="help-block">{{ e($errors->first('afm')) }}</p> @endif
        </div>
    </div>
    <!-- /.col-lg-4 -->
    <div class="col-lg-4">
        <div class="form-group @if ($errors->has('doy')) has-error @endif">
            {!! Form::label('doy', e(trans("language.customers_doy")), array('class' => 'control-label')) !!}
            {!! Form::text('doy', Input::old('doy'), array('class' => 'form-control', 'placeholder' => e(trans('language.customers_doy')), 'required' => 'required')) !!}
            @if ($errors->has('doy')) <p class="help-block">{{ e($errors->first('doy')) }}</p> @endif
        </div>
    </div>
    <!-- /.col-lg-4 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-4">
        <div class="form-group @if ($errors->has('phone_primary')) has-error @endif">
            {!! Form::label('phone_primary', e(trans("language.customers_phone")), array('class' => 'control-label')) !!}
            {!! Form::text('phone_primary', Input::old('phone_primary'), array('class' => 'form-control', 'placeholder' => e(trans('language.customers_phone')), 'required' => 'required')) !!}
            @if ($errors->has('phone_primary')) <p class="help-block">{{ e($errors->first('phone_primary')) }}</p> @endif
        </div>
    </div>
    <!-- /.col-lg-4 -->
    <div class="col-lg-4">
        <div class="form-group @if ($errors->has('phone_secondary')) has-error @endif">
            {!! Form::label('phone_secondary', e(trans("language.customers_phone_sec")), array('class' => 'control-label')) !!} <em>({{ e(trans('language.form_optional')) }})</em>
            {!! Form::text('phone_secondary', Input::old('phone_secondary'), array('class' => 'form-control', 'placeholder' => e(trans('language.customers_phone_sec')))) !!}
            @if ($errors->has('phone_secondary')) <p class="help-block">{{ e($errors->first('phone_secondary')) }}</p> @endif
        </div>
    </div>
    <!-- /.col-lg-4 -->
    <div class="col-lg-4">
        <div class="form-group @if ($errors->has('email')) has-error @endif">
            {!! Form::label('email', e(trans("language.customers_email")), array('class' => 'control-label')) !!} <em>({{ e(trans('language.form_optional')) }})</em>
            {!! Form::text('email', Input::old('email'), array('class' => 'form-control', 'placeholder' => e(trans('language.customers_email')))) !!}
            @if ($errors->has('email')) <p class="help-block">{{ e($errors->first('email')) }}</p> @endif
        </div>
    </div>
    <!-- /.col-lg-4 -->
</div>
<!-- /.row -->