@extends('app')

@section('page-title', e(trans('language.main_title') . ': ' . trans('language.sidebar_customers')))

{{-- Additional CSS files --}}
@section('top_css_files')
<!-- DataTables CSS -->
<link href="{{ asset('plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet">
@endsection

@section('header')
<h1>{{ e(trans('language.sidebar_customers')) }}</h1>
{!! Breadcrumbs::render('customers') !!}
@endsection

{{-- Main html --}}
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-header">
                <a href="{{ url('/dashboard/customers/create') }}" class="btn btn-success pull-left"><i class="fa fa-plus"></i>&nbsp;&nbsp;{{ e(trans('language.customers_add_new')) }}</a>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">{{ e(trans('language.customers_table')) }}</h3>
            </div>
            <div class="box-body">
                @if (!$customers->isEmpty())
                    <table class="table table-striped table-bordered table-hover" id="dataTable">
                        <thead>
                        <tr>
                            <th>{{ e(trans('language.customers_name')) }}</th>
                            <th>{{ e(trans('language.customers_phone_view')) }}</th>
                            <th>{{ e(trans('language.customers_afm')) }}</th>
                            <th>{{ e(trans('language.customers_doy')) }}</th>
                            <th>{{ e(trans('language.address_city')) }}</th>
                            <th>{{ e(trans('language.customers_transport')) }}</th>
                            <th class="nosort" style="width: 43px"></th>
                            <th class="nosort" style="width: 43px"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $odd = true ?>
                        @foreach ($customers as $customer)
                            @if ($odd)
                                <?php $class = 'odd' ?>
                            @else
                                <?php $class = 'even' ?>
                            @endif
                            <?php $odd = !$odd ?>
                            <tr class="{{ $class }}">
                                <td><a href="{{ url('/dashboard/customers/edit/' . e($customer['id'])) }}">{{ e($customer['last_name']) . ' ' . e($customer['first_name']) }}</a></td>
                                <td>{{ e($customer['phone_primary']) }}</td>
                                <td>{{ e($customer['afm']) }}</td>
                                <td>{{ e($customer['doy']) }}</td>
                                <td>{{ e($customer['address']['city']) }}</td>
                                <td>@if ($customer['transport']['id']) <a href="{{ url('/dashboard/transports/edit/' . e($customer['transport']['id'])) }}">{{ e($customer['transport']['name']) }}</a> @else {{ e('-') }} @endif</td>
                                <td>
                                    <div class="tooltip-buttons">
                                        <a href="{{ url('/dashboard/customers/edit/' . e($customer['id'])) }}" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="{{ e(trans('language.form_edit')) }}"><i class="fa fa-edit"></i></a>
                                    </div>
                                </td>
                                <td>
                                    <button class="btn btn-danger" data-toggle="modal" data-target="#modal" data-dataid="{{ e($customer['id']) }}"><i class="fa fa-trash"></i></button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    @include('partials._modal', array(
                        'class' => 'danger',
                        'title' => e(trans("language.modal_delete_title")),
                        'body' => e(trans("language.modal_delete_body_customer")),
                        'button' => e(trans("language.modal_delete")),
                        )
                    )
                @else
                    <div class="alert alert-info">
                        <h4><i class="icon fa fa-info"></i></h4>
                        {{ e(trans('language.customers_empty_warning')) }}.
                    </div>
                @endif
            </div>
            <!-- /.panel-body -->
            <div class="box-footer">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="pull-left">
                            <a href="{{ url('/dashboard/customers/create') }}" class="btn btn-success pull-left"><i class="fa fa-plus"></i>&nbsp;&nbsp;{{ e(trans('language.customers_add_new')) }}</a>
                        </div>
                    </div>
                </div>
            </div>
            @include('partials._request_wait_message')
        </div>
    </div>
</div>
@endsection

{{-- Additional JS files --}}
@section('body_js_libraries')
<!-- DataTables JavaScript -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('plugins/datatables/dataTables.bootstrap.js') }}"></script>
@endsection

{{-- Additional js script --}}
@section('body_bottom_js')
    @include('javascripts.datatables')
    @include('javascripts.modal_ajax_request', array('url' => '/dashboard/customers/delete/'))
@endsection