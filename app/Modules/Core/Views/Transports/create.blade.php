@extends('app')

@section('page-title', e(trans('language.main_title') . ': ' . trans('language.transports_form_new')))

@section('header')
    <h1>{{ e(trans('language.sidebar_transports')) }}</h1>
    {!! Breadcrumbs::render('transports_create') !!}
@endsection

{{-- Main html --}}
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">{{ e(trans('language.transports_form_new')) }}</h3>
                </div>
                {!! Form::open(array('role' => 'form', 'id' => 'submitForm', 'autocomplete' => 'off', 'method' => 'POST', 'url' => '/dashboard/transports/store')) !!}
                    <div class="box-body">
                        @include('Core.Transports::_form')
                        @include('partials._address')
                    </div>
                    <!-- /.panel-body -->
                    <div class="box-footer">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="pull-left">
                                    <button type="submit" class="btn btn-primary submit">{{ e(trans('language.form_submit')) }}</button>
                                    <button type="reset" class="btn btn-default reset">{{ e(trans('language.form_reset')) }}</button>
                                    <a href="{{ url('/dashboard/transports') }}">{{ e(trans('language.form_cancel')) }}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
@endsection

{{-- Additional js script --}}
@section('body_bottom_js')
    @include('javascripts._form_buttons')
    {!! $validator !!}
@endsection