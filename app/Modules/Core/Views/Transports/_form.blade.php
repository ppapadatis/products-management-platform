<div class="row">
    <div class="col-lg-6">
        <div class="form-group @if ($errors->has('name')) has-error @endif">
            {!! Form::label('name', e(trans("language.transports_name")), array('class' => 'control-label')) !!}
            {!! Form::text('name', Input::old('name'), array('class' => 'form-control', 'placeholder' => e(trans('language.transports_name')), 'autofocus' => 'autofocus', 'required' => 'required')) !!}
            @if ($errors->has('name')) <p class="help-block">{{ e($errors->first('name')) }}</p> @endif
        </div>
    </div>
    <!-- /.col-lg-6 -->
    <div class="col-lg-6">
        <div class="form-group @if ($errors->has('phone')) has-error @endif">
            {!! Form::label('phone', e(trans("language.transports_phone")), array('class' => 'control-label')) !!} <em>({{ e(trans('language.form_optional')) }})</em>
            {!! Form::text('phone', Input::old('phone'), array('class' => 'form-control', 'placeholder' => e(trans('language.transports_phone')))) !!}
            @if ($errors->has('phone')) <p class="help-block">{{ e($errors->first('phone')) }}</p> @endif
        </div>
    </div>
    <!-- /.col-lg-6 -->
</div>