@extends('app')

@section('page-title', e(trans('language.main_title') . ': ' . trans('language.transports_edit')))

@section('header')
    <h1>{{ e(trans('language.sidebar_transports')) }}</h1>
    {!! Breadcrumbs::render('transports_edit', $transport) !!}
@endsection

{{-- Main html --}}
@section('content')
    <div class="row">
        <div class="col-lg-12">
            @if (isset($not_found))
                <div class="alert alert-warning">
                    <h4><i class="icon fa fa-warning"></i></h4>
                    {{ e(trans('language.transports_not_found_warning')) }}
                </div>
                <a href="{{ url('/dashboard/transports') }}" class="btn btn-primary">{{ e(trans('language.form_return')) }}</a>
            @else
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">{{ e(trans('language.transports_edit')) }}</h3>
                    </div>
                    <!-- /.box-heading -->
                    {!! Form::model($transport, array('role' => 'form', 'id' => 'submitForm', 'autocomplete' => 'off', 'method' => 'PUT', 'url' => '/dashboard/transports/update/' . e($transport->id))) !!}
                    <div class="box-body">
                        @include('Core.Transports::_form')
                        @include('partials._address')
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="pull-left">
                                    <button type="submit" class="btn btn-primary submit">{{ e(trans('language.form_submit')) }}</button>
                                    <button type="reset" class="btn btn-default reset">{{ e(trans('language.form_reset')) }}</button>
                                    <a href="{{ url('/dashboard/transports') }}">{{ e(trans('language.form_cancel')) }}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
                <!-- /.box -->
            @endif
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
@endsection

{{-- Additional js script --}}
@section('body_bottom_js')
    @include('javascripts._form_buttons')
    {!! $validator !!}
@endsection