@extends('app')

@section('page-title', e(trans('language.main_title') . ': ' . trans('language.sidebar_profile')))

@section('top_css_files')
    <link href="{{ asset('plugins/bootstrap-switch/switch/bootstrap3/bootstrap-switch.min.css') }}" rel="stylesheet">
@endsection

@section('header')
    <h1>{{ e(trans('language.sidebar_profile')) }}</h1>
    {!! Breadcrumbs::render('profile') !!}
@endsection

{{-- Main html --}}
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">{{ e(trans('language.profile_manage')) }}</h3>
                </div>
                <!-- /.box-heading -->
                {!! Form::model($user, array('role' => 'form', 'id' => 'submitForm', 'autocomplete' => 'off', 'method' => 'PUT', 'url' => '/dashboard/profile/update/')) !!}
                <div class="box-body">
                    @include('Core.Profile::_form')
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="pull-left">
                                <button type="submit" class="btn btn-primary submit" disabled>{{ e(trans('language.form_submit')) }}</button>
                                <button type="reset" class="btn btn-default reset" disabled>{{ e(trans('language.form_reset')) }}</button>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
@endsection

@section('body_js_libraries')
    <script src="{{ asset('plugins/bootstrap-switch/bootstrap-switch.min.js') }}"></script>
@endsection

{{-- Additional js script --}}
@section('body_bottom_js')
    @include('javascripts._form_profile')
    {!! $validator !!}
@endsection