<div class="row">
    <div class="col-lg-12">
        <div class="form-group">
            {!! Form::label('allow', e(trans("language.form_allow_edit")), array('class' => 'control-label')) !!}
            <div class="checkbox">
                {!! Form::checkbox('allow', Input::old('allow'), array('value' => 'OFF')) !!}
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
        <div class="form-group @if ($errors->has('name')) has-error @endif">
            {!! Form::label('name', e(trans("language.profile_name")), array('class' => 'control-label')) !!}
            {!! Form::text('name', Input::old('name'), array('class' => 'form-control', 'placeholder' => e(trans('language.profile_name')), 'required' => 'required', 'disabled' => 'disabled')) !!}
            @if ($errors->has('name')) <p class="help-block">{{ e($errors->first('name')) }}</p> @endif
        </div>
    </div>
    <div class="col-lg-6">
        <div class="form-group @if ($errors->has('email')) has-error @endif">
            {!! Form::label('email', e(trans("language.profile_email")), array('class' => 'control-label')) !!}
            {!! Form::input('email', 'email', Input::old('email'), array('class' => 'form-control', 'placeholder' => e(trans('language.profile_email')), 'required' => 'required', 'disabled' => 'disabled')) !!}
            @if ($errors->has('email')) <p class="help-block">{{ e($errors->first('email')) }}</p> @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
        <div class="form-group @if ($errors->has('password')) has-error @endif">
            {!! Form::label('password', e(trans("language.profile_password")), array('class' => 'control-label')) !!}
            {!! Form::password('password', array('class' => 'form-control', 'placeholder' => e(trans('language.profile_password')), 'required' => 'required', 'disabled' => 'disabled')) !!}
            @if ($errors->has('password')) <p class="help-block">{{ e($errors->first('password')) }}</p> @endif
        </div>
    </div>
    <div class="col-lg-6">
        <div class="form-group @if ($errors->has('password_confirmation')) has-error @endif">
            {!! Form::label('password_confirmation', e(trans("language.profile_password_repeat")), array('class' => 'control-label')) !!}
            {!! Form::password('password_confirmation', array('class' => 'form-control', 'placeholder' => e(trans('language.profile_password_repeat')), 'required' => 'required', 'disabled' => 'disabled')) !!}
            @if ($errors->has('password_confirmation')) <p class="help-block">{{ e($errors->first('password_confirmation')) }}</p> @endif
        </div>
    </div>
</div>
<!-- /.row -->