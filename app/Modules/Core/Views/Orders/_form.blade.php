<div class="row">
    <div class="col-lg-6">
        <div class="form-group @if ($errors->has('customer_id')) has-error @endif">
            {!! Form::label('customer_id', e(trans("language.orders_customer")), array('class' => 'control-label')) !!}
            <div class="chosenContainer">
                {!! Form::select('customer_id', $customerArray, Input::old('customer_id'), array('class' => 'form-control chosen positive', 'data-placeholder' => e(trans('language.orders_customer')), 'required' => 'required')) !!}
            </div>
            @if ($errors->has('customer_id')) <p class="help-block">{{ e($errors->first('customer_id')) }}</p> @endif
        </div>
    </div>
    <div class="col-lg-3">
        <div class="form-group @if ($errors->has('discount')) has-error @endif">
            {!! Form::label('discount', e(trans("language.orders_discount")), array('class' => 'control-label')) !!} <em>({{ e(trans('language.form_optional')) }})</em>
            <div class="input-group">
                <span class="input-group-addon">%</span>
                {!! Form::input('number', 'discount', Input::old('discount'), array('class' => 'form-control', 'placeholder' => e(trans('language.orders_discount')))) !!}
            </div>
            @if ($errors->has('discount')) <p class="help-block">{{ e($errors->first('discount')) }}</p> @endif
        </div>
    </div>
    <div class="col-lg-3">
        <div class="form-group @if ($errors->has('vat')) has-error @endif">
            {!! Form::label('vat', e(trans("language.orders_vat")), array('class' => 'control-label')) !!} <em>({{ e(trans('language.form_optional')) }})</em>
            <div class="input-group">
                <span class="input-group-addon">%</span>
                {!! Form::input('number', 'vat', Input::old('vat'), array('class' => 'form-control', 'placeholder' => e(trans('language.orders_vat')))) !!}
            </div>
            @if ($errors->has('vat')) <p class="help-block">{{ e($errors->first('vat')) }}</p> @endif
        </div>
    </div>
</div>
<div class="clonedInput" id="entry1">
    <div class="row">
        <div class="col-lg-6">
            <div class="form-group @if ($errors->has('product_id')) has-error @endif">
                {!! Form::label("product", e(trans("language.orders_product")), array("class" => "control-label product-label")) !!}
                <div class="chosenContainer">
                    {!! Form::select("product1", $productArray, Input::old("product_id"), array("class" => "form-control chosen product-input positive", "data-placeholder" => e(trans("language.orders_product")), 'required' => 'required')) !!}
                </div>
                @if ($errors->has('product_id')) <p class="help-block">{{ e($errors->first('product_id')) }}</p> @endif
            </div>
        </div>
        <div class="col-lg-2">
            <div class="form-group @if ($errors->has('quantity')) has-error @endif">
                {!! Form::label("quantity", e(trans("language.orders_quantity")), array("class" => "control-label quantity-label")) !!}
                {!! Form::input('number', "quantity1", Input::old("quantity"), array("class" => "form-control quantity-input", "required" => "required", "placeholder" => e(trans("language.orders_quantity")))) !!}
                @if ($errors->has('quantity')) <p class="help-block">{{ e($errors->first('quantity')) }}</p> @endif
            </div>
        </div>
        <div class="col-lg-2">
            <div class="form-group @if ($errors->has('color')) has-error @endif">
                {!! Form::label("color", e(trans("language.orders_color")), array("class" => "control-label color-label")) !!}
                {!! Form::input("color", "color1", Input::old("color"), array("class" => "form-control color-input", "data-placeholder" => e(trans("language.orders_color")))) !!}
                @if ($errors->has('color')) <p class="help-block">{{ e($errors->first('color')) }}</p> @endif
            </div>
        </div>
        <div class="col-lg-2">
            <div class="form-group @if ($errors->has('size_id')) has-error @endif">
                {!! Form::label("size", e(trans("language.orders_size")), array("class" => "control-label size-label")) !!}
                <div class="chosenContainer">
                    {!! Form::select("size1", $sizeArray, Input::old("size_id"), array("class" => "form-control chosen size-input positive", "data-placeholder" => e(trans("language.orders_size")), 'required' => 'required')) !!}
                </div>
                @if ($errors->has('size_id')) <p class="help-block">{{ e($errors->first('size_id')) }}</p> @endif
            </div>
        </div>
        <div class="col-lg-12">
            <div class="form-group">
                {!! Form::label("notes", e(trans("language.orders_notes")), array("class" => "control-label notes-label")) !!}
                {!! Form::textarea('notes1', Input::old('notes'), array(
                'class' => 'form-control notes-input',
                'placeholder' => e(trans('language.orders_notes')),
                'style' => 'resize: none',
                'rows' => '2',
                )) !!}
            </div>
        </div>
        <div class="col-lg-12">
            <div class="form-group">
                <button type="button" class="btn btn-danger remove-btn"><i class="fa fa-minus"></i> {{ e(trans('language.form_remove_more') )}}</button>
            </div>
        </div>
    </div>
</div>