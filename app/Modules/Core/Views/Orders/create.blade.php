@extends('app')

@section('page-title', e(trans('language.main_title') . ': ' . trans('language.orders_form_new')))

@section('top_css_files')
    <link href="{{ asset('plugins/chosen/chosen.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('header')
    <h1>{{ e(trans('language.sidebar_orders')) }}</h1>
    {!! Breadcrumbs::render('orders_create') !!}
@endsection

{{-- Main html --}}
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">{{ e(trans('language.orders_form_new')) }}</h3>
                </div>
                {!! Form::open(array('role' => 'form', 'id' => 'submitForm', 'autocomplete' => 'off', 'method' => 'POST', 'url' => '/dashboard/orders/store')) !!}
                    <div class="box-body">
                        @include('Core.Orders::_form')
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <button type="button" class="btn btn-success add-btn"><i class="fa fa-plus"></i> {{ e(trans('language.form_add_more')) }}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.panel-body -->
                    <div class="box-footer">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="pull-left">
                                    <button type="submit" class="btn btn-primary submit">{{ e(trans('language.form_submit')) }}</button>
                                    <button type="reset" class="btn btn-default reset">{{ e(trans('language.form_reset')) }}</button>
                                    <a href="{{ url('/dashboard/orders') }}">{{ e(trans('language.form_cancel')) }}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
@endsection

@section('body_js_libraries')
    <script src='{{ asset('plugins/chosen/chosen.jquery.min.js') }}'></script>
@endsection

{{-- Additional js script --}}
@section('body_bottom_js')
    $('.chosen').chosen({no_results_text: "{{ e(trans('language.chosen_no_results')) }}"});
    @include('javascripts._form_buttons')
    {!! $validator !!}
    @include('javascripts._form_orders_dynamic')
@endsection