@extends('app')

@section('page-title', e(trans('language.main_title') . ': ' . trans('language.orders_edit')))

@section('top_css_files')
    <link href="{{ asset('plugins/chosen/chosen.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('header')
    <h1>{{ e(trans('language.sidebar_orders')) }}</h1>
    {!! Breadcrumbs::render('orders_edit', $order) !!}
@endsection

{{-- Main html --}}
@section('content')
    <div class="row">
        <div class="col-lg-12">
            @if (isset($not_found))
                <div class="alert alert-warning">
                    <h4><i class="icon fa fa-warning"></i></h4>
                    {{ e(trans('language.orders_empty_warning')) }}
                </div>
                <a href="{{ url('/dashboard/orders') }}" class="btn btn-primary">{{ e(trans('language.form_return')) }}</a>
            @else
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">{{ e(trans('language.orders_edit')) }}</h3>
                    </div>
                    <!-- /.box-heading -->
                    {!! Form::model($order, array('role' => 'form', 'id' => 'submitForm', 'autocomplete' => 'off', 'method' => 'PUT', 'url' => '/dashboard/orders/update/' . e($order->id))) !!}
                    <div class="box-body">
                        @include('Core.Orders::_form_edit')
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <button type="button" class="btn btn-success add-btn"><i class="fa fa-plus"></i> {{ e(trans('language.form_add_more')) }}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="pull-left">
                                    <button type="submit" class="btn btn-primary submit">{{ e(trans('language.form_submit')) }}</button>
                                    <button type="reset" class="btn btn-default reset">{{ e(trans('language.form_reset')) }}</button>
                                    <a href="{{ url('/dashboard/orders') }}">{{ e(trans('language.form_cancel')) }}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
                <!-- /.box -->
            @endif
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
@endsection

@section('body_js_libraries')
    <script src='{{ asset('plugins/chosen/chosen.jquery.min.js') }}'></script>
@endsection

{{-- Additional js script --}}
@section('body_bottom_js')
    $('.chosen').chosen({no_results_text: "{{ e(trans('language.chosen_no_results')) }}"});
    @include('javascripts._form_buttons')
    {!! $validator !!}
    @include('javascripts._form_orders_dynamic')
@endsection