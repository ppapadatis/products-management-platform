@extends('app')

@section('page-title', e(trans('language.main_title') . ': ' . trans('language.sidebar_orders')))

{{-- Additional CSS files --}}
@section('top_css_files')
    <!-- DataTables CSS -->
    <link href="{{ asset('plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet">
@endsection

@section('header')
<h1>{{ e(trans('language.sidebar_orders')) }}</h1>
{!! Breadcrumbs::render('orders') !!}
@endsection

{{-- Main html --}}
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-header">
                <a href="{{ url('/dashboard/orders/create') }}" class="btn btn-success pull-left"><i class="fa fa-plus"></i>&nbsp;&nbsp;{{ e(trans('language.orders_add_new')) }}</a>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">{{ e(trans('language.orders_table')) }}</h3>
            </div>
            <div class="box-body">
                @if (!$orders->isEmpty())
                    <table class="table table-striped table-bordered table-hover" id="dataTable">
                    <thead>
                    <tr>
                        <th class="nosort" style="width: 10px">#</th>
                        <th>{{ e(trans('language.orders_customer')) }}</th>
                        <th>{{ e(trans('language.orders_discount')) }}</th>
                        <th>{{ e(trans('language.orders_vat')) }}</th>
                        <th>{{ e(trans('language.orders_transport')) }}</th>
                        <th>{{ e(trans('language.orders_product')) }}</th>
                        <th>{{ e(trans('language.orders_date')) }}</th>
                        <th class="nosort" style="width: 43px"></th>
                        <th class="nosort" style="width: 43px"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $odd = true ?>
                    @foreach ($orders as $order)
                        @if ($odd)
                            <?php $class = 'odd' ?>
                        @else
                            <?php $class = 'even' ?>
                        @endif
                        <?php $odd = !$odd ?>
                        <tr class="{{ $class }}">
                            <td>{{ e($order['id']) }}</td>
                            <td><a href="{{ url('/dashboard/customers/edit/' . e($order['customer_id'])) }}">{{ e($order['customer']['first_name'] . ' ' . $order['customer']['last_name']) }}</a></td>
                            <td>{{ !empty($order['discount']) ? e($order['discount'] . ' %') : e('-') }}</td>
                            <td>{{ !empty($order['vat']) ? e($order['vat'] . ' %') : e('-')}}</td>
                            @if (!empty($order['customer']['transport_id']))
                                <td><a href="{{ url('/dashboard/transports/edit/' . e($order['customer']['transport_id'])) }}">{{ e($order['customer']['transport']['name']) }}</a></td>
                            @else
                                <td>{{ e('-') }}</td>
                            @endif
                            <td>{!! $order->getProductLinks() !!}</td>
                            <td><i class="fa fa-clock-o"></i> {{ e(date_format($order['created_at'], 'H:i:s d/m/Y')) }}</td>
                            <td>
                                <div class="tooltip-buttons">
                                    <a href="{{ url('/dashboard/orders/edit/' . e($order->id)) }}" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="{{ e(trans('language.form_edit')) }}"><i class="fa fa-edit"></i></a>
                                </div>
                            </td>
                            <td>
                                <button class="btn btn-danger" data-toggle="modal" data-target="#modal" data-dataid="{{ e($order['id']) }}"><i class="fa fa-trash"></i></button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    </table>
                    @include('partials._modal', array(
                        'class' => 'danger',
                        'title' => e(trans("language.modal_delete_title")),
                        'body' => e(trans("language.modal_delete_body_order")),
                        'button' => e(trans("language.modal_delete")),
                        )
                    )
                @else
                    <div class="alert alert-info">
                        <h4><i class="icon fa fa-info"></i></h4>
                        {{ e(trans('language.orders_empty_warning')) }}.
                    </div>
                @endif
            </div>
            <!-- /.panel-body -->
            <div class="box-footer">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="pull-left">
                            <a href="{{ url('/dashboard/orders/create') }}" class="btn btn-success pull-left"><i class="fa fa-plus"></i>&nbsp;&nbsp;{{ e(trans('language.orders_add_new')) }}</a>
                        </div>
                    </div>
                </div>
            </div>
            @include('partials._request_wait_message')
        </div>
    </div>
</div>
@endsection

{{-- Additional JS files --}}
@section('body_js_libraries')
    <!-- DataTables JavaScript -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('plugins/datatables/dataTables.bootstrap.js') }}"></script>
@endsection

{{-- Additional js script --}}
@section('body_bottom_js')
    @include('javascripts.datatables')
    @include('javascripts.modal_ajax_request', array('url' => '/dashboard/orders/delete/'))
@endsection