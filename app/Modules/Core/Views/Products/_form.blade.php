<div class="row">
    <div class="col-lg-6">
        <div class="form-group @if ($errors->has('sku')) has-error @endif">
            {!! Form::label('sku', e(trans("language.products_sku")), array('class' => 'control-label')) !!}
            {!! Form::text('sku', Input::old('sku'), array('class' => 'form-control', 'placeholder' => e(trans('language.products_sku')), 'autofocus' => 'autofocus', 'required' => 'required')) !!}
            @if ($errors->has('sku')) <p class="help-block">{{ e($errors->first('sku')) }}</p> @endif
        </div>
    </div>
    <!-- /.col-lg-6 -->
    <div class="col-lg-4">
        <div class="form-group @if ($errors->has('fabric')) has-error @endif">
            {!! Form::label('fabric', e(trans("language.products_fabric")), array('class' => 'control-label')) !!} <em>({{ e(trans('language.form_optional')) }})</em>
            {!! Form::text('fabric', Input::old('fabric'), array('class' => 'form-control', 'placeholder' => e(trans('language.products_fabric')))) !!}
            @if ($errors->has('fabric')) <p class="help-block">{{ e($errors->first('fabric')) }}</p> @endif
        </div>
    </div>
    <!-- /.col-lg-4 -->
    <div class="col-lg-2">
        <div class="form-group @if ($errors->has('wholesale_price')) has-error @endif">
            {!! Form::label('wholesale_price', e(trans("language.products_wholesale_price")), array('class' => 'control-label')) !!}
            <div class="input-group">
                <span class="input-group-addon">€</span>
                {!! Form::input('number', 'wholesale_price', Input::old('wholesale_price'), array('class' => 'form-control', 'placeholder' => e(trans('language.products_wholesale_price')), 'required' => 'required')) !!}
            </div>
            @if ($errors->has('wholesale_price')) <p class="help-block">{{ e($errors->first('wholesale_price')) }}</p> @endif
        </div>
    </div>
    <!-- /.col-lg-2 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="form-group @if ($errors->has('description')) has-error @endif">
            {!! Form::label('description', e(trans("language.products_description")), array('class' => 'control-label')) !!}
            {!! Form::textarea('description', Input::old('description'), array(
                'class' => 'form-control',
                'maxlength' => '255',
                'id' => 'description',
                'placeholder' => e(trans('language.products_description')),
                'style' => 'resize: none',
                'rows' => '2',
                'required' => 'required'
            )) !!}
            <div id="textarea_feedback" class="pull-right"></div>
        @if ($errors->has('description')) <p class="help-block">{{ e($errors->first('description')) }}</p> @endif
        </div>
    </div>
</div>
<!-- /.row -->