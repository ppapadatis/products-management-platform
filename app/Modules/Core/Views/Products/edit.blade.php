@extends('app')

@section('page-title', e(trans('language.main_title') . ': ' . trans('language.products_edit')))

@section('header')
    <h1>{{ e(trans('language.sidebar_products')) }}</h1>
    {!! Breadcrumbs::render('products_edit', $product) !!}
@endsection

{{-- Main html --}}
@section('content')
    <div class="row">
        <div class="col-lg-12">
            @if (isset($not_found))
                <div class="alert alert-warning">
                    <h4><i class="icon fa fa-warning"></i></h4>
                    {{ e(trans('language.products_not_found_warning')) }}
                </div>
                <a href="{{ url('/dashboard/products') }}" class="btn btn-primary">{{ e(trans('language.form_return')) }}</a>
            @else
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">{{ e(trans('language.products_edit')) }}</h3>
                    </div>
                    <!-- /.box-heading -->
                    {!! Form::model($product, array('role' => 'form', 'id' => 'submitForm', 'autocomplete' => 'off', 'method' => 'PUT', 'url' => '/dashboard/products/update/' . e($product->id))) !!}
                    <div class="box-body">
                        @include('Core.Products::_form')
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="pull-left">
                                    <button type="submit" class="btn btn-primary submit">{{ e(trans('language.form_submit')) }}</button>
                                    <button type="reset" class="btn btn-default reset">{{ e(trans('language.form_reset')) }}</button>
                                    <a href="{{ url('/dashboard/products') }}">{{ e(trans('language.form_cancel')) }}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
                <!-- /.box -->
            @endif
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
@endsection

{{-- Additional js script --}}
@section('body_bottom_js')
    @include('javascripts._textarea_count')
    @include('javascripts._form_buttons')
    {!! $validator !!}
@endsection