@extends('app')

@section('page-title', e(trans('language.main_title') . ': ' . trans('language.sidebar_products')))

{{-- Additional CSS files --}}
@section('top_css_files')
<!-- DataTables CSS -->
<link href="{{ asset('plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet">
@endsection

@section('header')
<h1>{{ e(trans('language.sidebar_products')) }}</h1>
{!! Breadcrumbs::render('products') !!}
@endsection

{{-- Main html --}}
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-header">
                <a href="{{ url('/dashboard/products/create') }}" class="btn btn-success pull-left"><i class="fa fa-plus"></i>&nbsp;&nbsp;{{ e(trans('language.products_add_new')) }}</a>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">{{ e(trans('language.products_table')) }}</h3>
            </div>
            <div class="box-body">
                @if (!$products->isEmpty())
                    <table class="table table-striped table-bordered table-hover" id="dataTable">
                        <thead>
                        <tr>
                            <th>{{ e(trans('language.products_sku')) }}</th>
                            <th>{{ e(trans('language.products_description')) }}</th>
                            <th>{{ e(trans('language.products_fabric')) }}</th>
                            <th>{{ e(trans('language.products_wholesale_price')) }}</th>
                            <th class="nosort" style="width: 43px"></th>
                            <th class="nosort" style="width: 43px"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $odd = true ?>
                        @foreach ($products as $product)
                            @if ($odd)
                                <?php $class = 'odd' ?>
                            @else
                                <?php $class = 'even' ?>
                            @endif
                            <?php $odd = !$odd ?>
                            <tr class="{{ $class }}">
                                <td><a href="{{ url('/dashboard/products/edit/' . e($product['id'])) }}">{{ e($product['sku']) }}</a></td>
                                <td>{{ e($product['description']) }}</td>
                                <td>{{ $product['fabric'] ? e($product['fabric']) : e('-') }}</td>
                                <td>{{ e('€ ') . e($product['wholesale_price']) }}</td>
                                <td>
                                    <div class="tooltip-buttons">
                                        <a href="{{ url('/dashboard/products/edit/' . e($product->id)) }}" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="{{ e(trans('language.form_edit')) }}"><i class="fa fa-edit"></i></a>
                                    </div>
                                </td>
                                <td>
                                    <button class="btn btn-danger" data-toggle="modal" data-target="#modal" data-dataid="{{ e($product['id']) }}"><i class="fa fa-trash"></i></button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    @include('partials._modal', array(
                        'class' => 'danger',
                        'title' => e(trans("language.modal_delete_title")),
                        'body' => e(trans("language.modal_delete_body_product")),
                        'button' => e(trans("language.modal_delete")),
                        )
                    )
                @else
                    <div class="alert alert-info">
                        <h4><i class="icon fa fa-info"></i></h4>
                        {{ e(trans('language.products_empty_warning')) }}.
                    </div>
                @endif
            </div>
            <!-- /.panel-body -->
            <div class="box-footer">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="pull-left">
                            <a href="{{ url('/dashboard/products/create') }}" class="btn btn-success pull-left"><i class="fa fa-plus"></i>&nbsp;&nbsp;{{ e(trans('language.products_add_new')) }}</a>
                        </div>
                    </div>
                </div>
            </div>
            @include('partials._request_wait_message')
        </div>
    </div>
</div>
@endsection

{{-- Additional JS files --}}
@section('body_js_libraries')
<!-- DataTables JavaScript -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('plugins/datatables/dataTables.bootstrap.js') }}"></script>
@endsection

{{-- Additional js script --}}
@section('body_bottom_js')
    @include('javascripts.datatables')
    @include('javascripts.modal_ajax_request', array('url' => '/dashboard/products/delete/'))
@endsection