@extends('app')

@section('page-title', e(trans('language.main_title') . ': ' . trans('language.products_form_new')))

@section('header')
    <h1>{{ e(trans('language.sidebar_products')) }}</h1>
    {!! Breadcrumbs::render('products_create') !!}
@endsection

{{-- Main html --}}
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">{{ e(trans('language.products_form_new')) }}</h3>
                </div>
                {!! Form::open(array('role' => 'form', 'id' => 'submitForm', 'autocomplete' => 'off', 'method' => 'POST', 'url' => '/dashboard/products/store')) !!}
                    <div class="box-body">
                        @include('Core.Products::_form')
                    </div>
                    <!-- /.panel-body -->
                    <div class="box-footer">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="pull-left">
                                    <button type="submit" class="btn btn-primary submit">{{ e(trans('language.form_submit')) }}</button>
                                    <button type="reset" class="btn btn-default reset">{{ e(trans('language.form_reset')) }}</button>
                                    <a href="{{ url('/dashboard/products') }}">{{ e(trans('language.form_cancel')) }}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
@endsection

{{-- Additional js script --}}
@section('body_bottom_js')
    @include('javascripts._textarea_count')
    @include('javascripts._form_buttons')
    {!! $validator !!}
@endsection