<?php namespace App\Modules\Core\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TransportCompany extends Model {

    use SoftDeletes;

    protected $table = 'transport_companies';

    public static $rules = array(
        'name' => 'required',
    );

    protected $dates = array('deleted_at');

    protected $primaryKey = 'id';

    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    protected $guarded = array('*');

    protected $fillable = array('name', 'phone');

    public function customers()
    {
        return $this->hasMany('\App\Modules\Core\Models\Customer', 'transport_id', 'id');
    }

    public function address()
    {
        return $this->hasOne('\App\Modules\Core\Models\Address', 'transport_id', 'id');
    }

}
