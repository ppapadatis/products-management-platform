<?php namespace App\Modules\Core\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model {

    use SoftDeletes;

    protected $table = 'orders';

    public static $rules = array(
        'customer_id' => 'required',
        'discount' => 'sometimes|numeric',
        'vat' => 'sometimes|numeric'
    );

    protected $dates = array('deleted_at');

    protected $primaryKey = 'id';

    protected $hidden = array('updated_at', 'deleted_at');

    protected $guarded = array('*');

    protected $fillable = array('customer_id', 'discount', 'vat');

    public function customer()
    {
        return $this->belongsTo('\App\Modules\Core\Models\Customer', 'customer_id');
    }

    public function orderItems()
    {
        return $this->hasMany('\App\Modules\Core\Models\OrderItem', 'order_id', 'id');
    }

    public function getProductLinks()
    {
        $productsArray = array();
        $orderItems = $this->orderItems()->getResults();
        foreach ($orderItems as $orderItem) {
            array_push($productsArray, '<a href="' . url('/dashboard/products/edit/' . $orderItem['product']['id']) . '">' . $orderItem['product']['sku'] . '</a>');
        }

        return !empty($productsArray) ? implode(', ', $productsArray) : '-';
    }

    public function getOrderItemsIds()
    {
        $orderItemsArray = array();
        $orderItems = $this->orderItems()->getResults();
        foreach ($orderItems as $orderItem) {
            array_push($orderItemsArray, $orderItem['id']);
        }

        return $orderItemsArray;
    }

}
