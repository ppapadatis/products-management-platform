<?php namespace App\Modules\Core\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderItem extends Model {

    use SoftDeletes;

    protected $table = 'order_items';

    public static $rules = array(
        'product_id' => 'required',
        'quantity' => 'required|integer',
        'size_id' => 'required',
    );

    protected $dates = array('deleted_at');

    protected $touches = array('order');

    protected $primaryKey = 'id';

    protected $hidden = array('updated_at', 'deleted_at');

    protected $guarded = array('*');

    protected $fillable = array('order_id', 'product_id', 'quantity', 'color', 'size_id', 'notes');

    public function size()
    {
        return $this->hasOne('\App\Modules\Core\Models\Size', 'id', 'size_id');
    }

    public function product()
    {
        return $this->hasOne('\App\Modules\Core\Models\Product', 'id', 'product_id');
    }

    public function order()
    {
        return $this->belongsTo('\App\Modules\Core\Models\Order', 'order_id');
    }

}
