<?php namespace App\Modules\Core\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model {

    use SoftDeletes;

	protected $table = 'customers';

    protected $dates = array('deleted_at');

    public static $rules = array(
        'first_name' => 'required',
        'last_name' => 'required',
        'afm' => 'required|numeric',
        'doy' => 'required',
        'phone_primary' => 'required',
        'email' => 'sometimes|email',
    );

    protected $primaryKey = 'id';

    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    protected $guarded = array('*');

    protected $fillable = array('first_name', 'last_name', 'distinctive_title', 'afm', 'doy', 'phone_primary', 'phone_secondary', 'email', 'address_id', 'transport_id');

    public function transport()
    {
        return $this->belongsTo('\App\Modules\Core\Models\TransportCompany', 'transport_id');
    }

    public function orders()
    {
        return $this->hasMany('\App\Modules\Core\Models\Order', 'customer_id', 'id');
    }

    public function address()
    {
        return $this->hasOne('\App\Modules\Core\Models\Address', 'customer_id', 'id');
    }

}
