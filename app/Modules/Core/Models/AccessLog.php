<?php namespace App\Modules\Core\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AccessLog extends Model {

    use SoftDeletes;

    protected $table = 'access_logs';

    protected $dates = array('deleted_at');

    protected $primaryKey = 'id';

    protected $hidden = array('updated_at', 'deleted_at');

    protected $guarded = array('*');

    protected $fillable = array('user_id', 'remote_ip', 'success', 'credentials');

    public function user()
    {
        return $this->belongsTo('\App\User', 'user_id');
    }

}
