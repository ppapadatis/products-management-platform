<?php namespace App\Modules\Core\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Address extends Model {

    use SoftDeletes;

    protected $table = 'addresses';

    protected $dates = array('deleted_at');

    protected $touches = array('customer', 'transportCompany');

    public static $rules = array(
        'street' => 'required',
        'number' => 'required',
        'postal' => 'required',
        'city' => 'required',
        'country' => 'required',
    );

    protected $primaryKey = 'id';

    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    protected $guarded = array('*');

    protected $fillable = array('street', 'number', 'postal', 'city', 'region', 'country');

    public function customer()
    {
        return $this->belongsTo('\App\Modules\Core\Models\Customer', 'customer_id');
    }

    public function transportCompany()
    {
        return $this->belongsTo('\App\Modules\Core\Models\TransportCompany', 'transport_id');
    }

}
