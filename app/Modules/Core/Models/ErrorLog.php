<?php namespace App\Modules\Core\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ErrorLog extends Model {

    use SoftDeletes;

    protected $table = 'error_logs';

    protected $dates = array('deleted_at');

    protected $primaryKey = 'id';

    protected $hidden = array('updated_at', 'deleted_at');

    protected $guarded = array('*');

    protected $fillable = array('level', 'message', 'details', 'user_agent', 'remote_ip', 'user_id');

    public function user()
    {
        return $this->belongsTo('\App\User', 'user_id');
    }

}
