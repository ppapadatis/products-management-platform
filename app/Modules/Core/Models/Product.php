<?php namespace App\Modules\Core\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model {

    use SoftDeletes;

    protected $table = 'products';

    public static $rules = array(
        'sku' => 'required',
        'description' => 'required',
        'wholesale_price' => 'required|numeric',
    );

    protected $dates = array('deleted_at');

    protected $primaryKey = 'id';

    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    protected $guarded = array('*');

    protected $fillable = array('sku', 'description', 'fabric', 'wholesale_price');

    public function orderItems()
    {
        return $this->belongsToMany('\App\Modules\Core\Models\OrderItem', 'order_items', 'product_id');
    }

}
