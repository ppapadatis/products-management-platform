<?php namespace App\Modules\Core\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Size extends Model {

    use SoftDeletes;

    protected $table = 'sizes';

    public static $rules = array(
        'name' => 'required'
    );

    protected $dates = array('deleted_at');

    protected $primaryKey = 'id';

    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    protected $guarded = array('*');

    protected $fillable = array('name');

    public function orderItems()
    {
        return $this->belongsToMany('\App\Modules\Core\Models\OrderItem', 'order_items', 'size_id');
    }

}
