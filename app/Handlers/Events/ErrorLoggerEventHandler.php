<?php namespace App\Handlers\Events;

use App\Modules\Core\Models\ErrorLog;
use Illuminate\Http\Request;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;

class ErrorLoggerEventHandler {

    protected $request;

	/**
	 * Create the event handler.
	 *
	 * @return void
	 */
	public function __construct(Request $request)
	{
		$this->request = $request;
	}

	/**
	 * Handle the event.
	 *
	 * @param  string  $level
	 * @param  object|string  $message
	 * @param  string  $context
	 * @return void
	 */
	public function handle($level, $message, $context)
	{
        if ($message instanceof \Exception) {
            $message = array(
                'message' => $message->getMessage(),
                'code' => $message->getCode(),
                'trace' => $message->getTraceAsString(),
                'file' => $message->getFile(),
                'line' => $message->getLine()
            );
        } else {
            $message = array(
                'message' => $message
            );
        }

        $data = array(
            'level' => $level,
            'message' => serialize($message),
            'details' => !empty($context) ? $context : $message['file'] . ' : ' . $message['line'],
            'user_agent' => isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : null,
            'remote_ip' => \Request::getClientIp(),
            'user_id' => \Auth::user() ? \Auth::user()->id : null
        );

        \Queue::push(function () use ($data) {
            ErrorLog::create($data);
        });
	}

}
