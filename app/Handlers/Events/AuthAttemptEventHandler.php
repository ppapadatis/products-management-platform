<?php namespace App\Handlers\Events;

use App\Modules\Core\Models\AccessLog;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use Illuminate\Http\Request;

class AuthAttemptEventHandler {

    protected $request;

	/**
	 * Create the event handler.
	 *
	 * @return void
	 */
	public function __construct(Request $request)
	{
		$this->request = $request;
	}

	/**
	 * Handle the event.
	 *
	 * @param  array  $email
	 * @return void
	 */
	public function handle($credentials)
	{
        $data = array(
            'user_id' => null,
            'remote_ip' => $this->request->getClientIp(),
            'success' => false,
            'credentials' => base64_encode(serialize($credentials)),
        );

        \Queue::push(function() use ($data) {
            AccessLog::create($data);
        });
	}

}
