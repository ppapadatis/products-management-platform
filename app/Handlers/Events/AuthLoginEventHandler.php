<?php namespace App\Handlers\Events;

use App\Modules\Core\Models\AccessLog;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use Illuminate\Http\Request;

class AuthLoginEventHandler {

    protected $request;

	/**
	 * Create the event handler.
	 *
	 * @return void
	 */
	public function __construct(Request $request)
	{
		$this->request = $request;
	}

	/**
	 * Handle the event.
	 *
	 * @param  \App\User  $user
     * @param  bool  $remember
	 * @return void
	 */
	public function handle($user, $remember)
	{
        $data = array(
            'user_id' => $user->id,
            'remote_ip' => $this->request->getClientIp(),
            'success' => true,
        );

        \Queue::push(function() use ($data) {
            AccessLog::create($data);
        });
	}

}
