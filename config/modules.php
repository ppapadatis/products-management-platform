<?php
return array(
    'Modules' => array(
        'Core' => array(
            'AccessLogs',
            'Colors',
            'Customers',
            'Dashboard',
            'ErrorLogs',
            'Orders',
            'Products',
            'Profile',
            'Settings',
            'Sizes',
            'Transports',
        ),
    ),
);