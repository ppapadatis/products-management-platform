<?php
return array(

    /**
     * Default view used to render Javascript validation code
     */
//    'view' => 'jsvalidation::bootstrap',
    'view' => 'jsvalidation::bootstrap_update',


    /**
     * Default JQuery selector find the form to be validated.
     * By default, the validations are applied to all forms.
     */
    'form_selector' => 'form',

);
